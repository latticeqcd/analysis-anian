"""Constants and parameters for ensembles (outdated)

References:

[1]     M. Della Morte et al., The hadronic vacuum polarization
        contribution to the muon g-2 from lattice QCD.
        https://arxiv.org/abs/1705.01775 (10.12.2021)

[2]     The NIST Reference on Constants, Units, and Uncertainty,
        Fundamental Physical Constants.
        https://physics.nist.gov/cgi-bin/cuu/Value?alph (10.12.2021)
        https://physics.nist.gov/cgi-bin/cuu/Value?mmuc2mev (10.12.2021)

[3]     Particle Data Group, Physical Constants.
        https://pdg.lbl.gov/2021/web/viewer.html?file=../reviews/rpp2021-rev-phys-constants.pdf (10.12.2021)

[4]     Jens Lücke, An update on QCD+QED simulations with C* boundary conditions,
        https://arxiv.org/pdf/2108.11989.pdf (08.04.2022)
"""
import os

import numpy as np

from measurement.measurement import M

ALPHA = 7.2973525693e-3  # see Ref. [2]
M_MU = 105.6583755  # MeV, see Ref. [2]
HBAR_C = 197.3269804  # MeV * fm, see Ref. [3]

def M_MU_LATTICE(a):
    """muon mass in lattice units."""
    if isinstance(a, M):
        return a.value * M_MU / HBAR_C
    return a * M_MU / HBAR_C


# charges squared
Q2 = {0: 4.0/9.0, 1: 1.0/9.0, 3: 4.0/9.0}

# renormalization of vector current, see Table 8 in [1].
# The arrays contains ZV for light and strange quarks.
ZV = {
      "A5": [M(0.72724, 0.00043), M(0.74803, 0.00021)],
      "E5": [M(0.74418, 0.00033), M(0.75829, 0.00022)],
      "G8": [M(0.73887, 0.00010), M(0.75983, 0.00013)],
    # fit range for QCD-32-1: [6, 18), [15, 25)
      "A400": [M(0.6792, 0.0009), M(0.6792, 0.0009), M(0.6792, 0.0009), M(0.6038, 0.004)],
    # fit ranges for QXD-32-2: [6, 18), [7, 19), [15, 25)
      "A360": [M(0.6739, 0.0017), M(0.6845, 0.0007), M(0.6845, 0.0007), M(0.5953, 0.0004)],
    # fit ranges for A380: [6, 18), [7, 19), [15, 25)
      "A380": [M(0.6811, 0.0010), M(0.6822, 0.0006), M(0.6822, 0.0006), M(0.6029, 0.0004)],
      # "A380": [M(0.6721, 0.0018), M(0.6798, 0.0007), M(0.6798, 0.0007), M(0.604, 0.00018)],  # Roman
      "ba": [M(1, 0), M(1, 0)],
      "qc": [M(1, 0), M(1, 0)],
      "qx": [M(1, 0), M(1, 0)],
}


# lattice constant, systematic and statistical error are added in quadrature,
# see Table 1, Ref. [1] for A5, E5
# see Table 2, Ref. [4] for QCD-32-1
# see Poster by Alessandro for QCD, QXD, A380
LATTICE_CONSTANT = {"A5": M(0.0755, np.sqrt(0.0009**2 + 0.0007**2)),
                    "E5": M(0.0658, np.sqrt(0.0007**2 + 0.0007**2)),
                    "G8": M(0.0658, np.sqrt(0.0007**2 + 0.0007**2)),
                    "A360": M(0.05393, 0.00024),
                    "A400": M(0.05054, 0.00027),
                    "A380": M(0.05323,0.00028),
                    "qcd": M(0.1, 0.0)}

KAPPA = {"A360": [0.13440733, 0.13440733, 0.13440733, 0.12784],
         "A400": [0.135560, 0.134617, 0.13617, 0.129583],
         "A380": [0.13459164, 0.13444333, 0.12806355]
}

# points for spline interpolation.
CONTINUUM_POINTS = 1000
INTEGRATION_CUTOFF = 1000  # in lattice units
INTEGRATION_POINTS = 10*INTEGRATION_CUTOFF
SAMPLES = 100  # number of repetitions to determine systematic errors (a, ZV, mass, ...)

ROOT = os.path.dirname(os.path.realpath(__file__))
csv_template = os.path.join(ROOT, "data", "results_template.csv")

DATDIR = os.path.join(ROOT, "..", "data")

# compare with Gattringer & Lang, Eq. (6.59)
modified_ampl = lambda N0, t, mass, ampl: 2 * ampl * np.exp(-mass*N0/2) * np.cosh((t-N0/2)*mass)
# compare with Gattringer & Lang, Eq. (6.55)
simple_ampl = lambda N0, t, mass, ampl: ampl * np.cosh((t-N0/2)*mass)

MODEL_FUNCTION = "modified"  # "simple" or "modified"

if MODEL_FUNCTION == "modified":
    cosh_template = modified_ampl
elif MODEL_FUNCTION == "simple":
    cosh_template = simple_ampl
