"""Definition of gamma matrices as used in openQ*D."""
import numpy as np

gammas = [np.zeros((4,4), dtype=complex) for k in range(6)]

gammas[0][0,2] = -1.0
gammas[0][1,3] = -1.0
gammas[0][2,0] = -1.0
gammas[0][3,1] = -1.0

gammas[1][0,3] = -1.0j
gammas[1][1,2] = -1.0j
gammas[1][2,1] = +1.0j
gammas[1][3,0] = +1.0j

gammas[2][0,3] = -1.0
gammas[2][1,2] = +1.0
gammas[2][2,1] = +1.0
gammas[2][3,0] = -1.0

gammas[3][0,2] = -1.0j
gammas[3][1,3] = +1.0j
gammas[3][2,0] = +1.0j
gammas[3][3,1] = -1.0j

gammas[4] = np.eye(4)

gammas[5] = np.diag([1.0,1.0,-1.0,-1.0])


C = 1.0j* gammas[2]@gammas[0]

if __name__ == "__main__":
    print(f"{C=}")
    print(C@C)
