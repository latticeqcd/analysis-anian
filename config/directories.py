"""contains constants needed in the whole directory."""
import os
import subprocess

import numpy as np

# pylint: disable=redefined-builtin

root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
RAW_DATA = os.path.join(root, "..", "raw-data")
DATA = os.path.join(root, "..", "dat")

CHARGES = {
    "u": 2 / 3,
    "d": -1 / 3,
    "c": 2 / 3,
    "s": -1 / 3,
    "ud": 1 / 3,
    "ds": -2 / 3,
    "dsc": 0,
}

ALPHA = 7.2973525693e-3  # see Ref. [2]
M_MU = 105.6583755  # MeV, see Ref. [2]
HBAR_C = 197.3269804  # MeV * fm, see Ref. [3]

try:
    GIT_COMMIT = (
        subprocess.check_output("git rev-parse HEAD".split()).decode().strip("\n")
    )
except subprocess.CalledProcessError as e:
    GIT_COMMIT = "no git repo"
    print(e.stderr)


def m_mu_lattice(a):
    """muon mass in lattice units."""
    return a * M_MU / HBAR_C


# pylint: disable=too-many-instance-attributes,too-many-arguments,too-few-public-methods
class Ensemble:
    """dictionary that maps ensemble to runname"""

    def __init__(
        self,
        name,
        runname,
        configs,
        flavors,
        cstar,
        dir=None,
        sources=None,
        volume=None,
        a=None,
        fit_range=None,
        cutoff=None,
    ):
        self.name = name
        self.runname = runname
        self.configs = configs
        self.flavors = flavors
        self.cstar = cstar
        if dir is None:
            dir = os.path.join(DATA, name)
        self.dir = dir
        if sources is None:
            sources = np.arange(10)
        self.sources = sources
        self.volume = volume
        self.a = a  # (value, error)
        self.fit_range = fit_range
        self.cutoff = cutoff
        if cstar == 0:
            self.physvolume = np.copy(volume)
        elif cstar > 0:
            self.physvolume = np.copy(volume)
            self.physvolume[1] /= 2

    def m_mu(self):
        """returns the muon mass in lattice units."""
        assert self.a, "latice spacing must be set"
        return m_mu_lattice(self.a[0])

    def phys_vol(self):
        vol = np.prod(self.volume)
        if self.cstar > 0:
            return vol / 2
        return vol


_ensembles_list = [
    Ensemble(
        "A360a50b324",
        "64x32b3.24a0.05ku0.135560kds0.134617kc0.129583r001",
        np.arange(500, 1405, 5),
        ["u", "ds", "c"],
        cstar=3,
        volume=[64, 64, 32, 32],
        a=(0.05393, 0.00024),
    ),
    Ensemble(
        "A380a07b324",
        "64x32b3.24a0.007299ku0.13459164kds0.13444333kc0.12806355r001",
        np.arange(10, 2010, 10),
        # ["u", "ds", "c"],
        ["u", "ds"],
        cstar=3,
        volume=[64, 64, 32, 32],
        a=(0.05323, 0.00028),
    ),
    Ensemble(
        "A400a00b324",
        "64x32b3.24a0.00ku0.134408kd0.134408ks0.134408kc0.127840r003",
        np.arange(10, 2010, 10),
        ["u", "dsc"],
        cstar=3,
        volume=[64, 64, 32, 32],
        a=(0.05054, 0.00027),
    ),
    Ensemble(
        "C380a50b324",
        "96x48b3.24a0.05ku0.1355368kds0.134596kc0.12959326r001",
        np.arange(100, 1336, 2),
        ["u", "ds", "c"],
        cstar=3,
        a=(0.050625, 0.000079),
        volume=[96, 96, 48, 48],
        fit_range=np.arange(20, 35),
        cutoff=32,
    ),
    Ensemble(
        "A5d",
        "64x32x32x32b5.20k0.13594c2.01715id4",
        np.arange(1, 501, 1),
        ["ud", "s"],
        cstar=0,
        volume=[64, 32, 32, 32],
        a=(0.0755, np.sqrt(0.0009**2 + 0.0007**2)),
    ),
    Ensemble(
        "A5c",
        "64x32x32x32b5.20k0.13594c2.01715id3",
        np.arange(2, 502, 2),
        ["ud", "s"],
        cstar=0,
        volume=[64, 32, 32, 32],
        a=(0.0755, np.sqrt(0.0009**2 + 0.0007**2)),
    ),
]

_ensembles_list += [
    Ensemble(
        f"qcd{i}",
        f"qcd{i}",
        np.arange(5, 105, 5),
        ["u"],
        cstar=int(bool(i)),
        volume=[16, 8, 8, 8] if i == 0 else [16, 16, 8, 8],
        dir=os.path.join(DATA, "qcd"),
        a=(0.10, 0.0),
    )
    for i in range(4)
]

_ensembles_list += [
    Ensemble(
        f"qxd{i}",
        f"qxd{i}",
        np.arange(5, 105, 5),
        ["u", "d"],
        cstar=int(bool(i)),
        volume=[16, 8, 8, 8] if i == 0 else [16, 16, 8, 8],
        dir=os.path.join(DATA, "qxd"),
        a=(0.10, 0.0),
    )
    for i in range(4)
]

_ensembles_list += [
    Ensemble(
        f"ib{i}",
        f"ib{i}",
        np.arange(11, 111, 1),
        ["ud", "s"],
        cstar=1,
        volume=[i, 2 * i, i, i],
        dir=os.path.join(DATA, "qed"),
        a=(0.10, 0.0),
    )
    for i in [8, 16, 24, 32]
]

ensembles = {el.name: el for el in _ensembles_list}
