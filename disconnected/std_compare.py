"""compares standard deviation of disconnected two-point function
for each flavor individually. Usage example:

    python -m disconnected.std_compare --ensemble A380a07b324
        --type hopping --ensemble 10 2010 10
"""
import os

import numpy as np
from scipy.stats import sem

from read.parse import arguments
from read.read_meson import read_array_file, read_meson_file, fold_correlator
from disconnected.generate_twopoint import twopoint_wrapper

eps = 1e-15

def compare_stds(ensemble, curr_type):
    """Compares standard deviation per flavor.

    We consider the variance
        a/T sum_{x0} sigma_{C(x0)}^2,
    where C(x0) is the two-point function.

    Args:
        ensemble (ensemble): ensemble
        curr_type (str): current type (corresponds to directory name)
    """
    onepoints = []
    twopoints = []
    two = [] # generated from one-point, flavor 0
    two0 = [] # generated from one-point, flavor 0
    two1 = [] # generated from one-point, flavor 1
    volume = np.prod(ensemble.volume)
    if ensemble.cstar > 0:
        volume /= 2
    for cnfg in ensemble.configs:
        base = os.path.join(ensemble.dir, curr_type, f"{ensemble.runname}n{cnfg}_")
        onepoints += [read_array_file(base + "onepoint" + ".dat")]
        two += [twopoint_wrapper(onepoints[-1], volume)]
        two0 += [twopoint_wrapper(onepoints[-1][:,:1,...], volume)]
        two1 += [twopoint_wrapper(onepoints[-1][:,1:,...], volume)]
        twopoints += [read_meson_file(base + "twopoint" + ".dat")]
        diff = np.linalg.norm(two[-1] - twopoints[-1])
        assert diff < eps
    one = np.array(onepoints)
    one = np.sum(one, axis=2) # sum flavors
    one = np.mean(one, axis=2) # average over gammas
    one = np.mean(one, axis=1) # average of sources
    std = sem(one, axis=0) # standard deviation over configs
    var = np.mean(std**2) # average std over time
    print(f"{'variance one-point': <25} {var:.3f}")

    two = np.array(two)
    two = np.transpose(fold_correlator(np.transpose(two)))
    std = sem(two, axis=0)
    var = np.mean(std**2) # average std over time
    print(f"{'variance two-point both': <25} {var:.2e}")

    two = np.array(two0)
    two = np.transpose(fold_correlator(np.transpose(two)))
    std = sem(two, axis=0)
    var = np.mean(std**2) # average std over time
    print(f"{'variance two-point up': <25} {var:.2e}")

    two = np.array(two1)
    two = np.transpose(fold_correlator(np.transpose(two)))
    std = sem(two, axis=0)
    var = np.mean(std**2) # average std over time
    print(f"{'variance two-point d/s': <25} {var:.2e}")



if __name__ == "__main__":
    args = arguments()
    if args.ensemble.name == "qxd0":
        args.ensemble.volume = [4, 4, 4, 4]
    for curr_type in args.type:
        compare_stds(args.ensemble, curr_type)
