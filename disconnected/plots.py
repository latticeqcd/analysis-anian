"""generate plots for disconnected contribution"""
import sys

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np

from config.directories import Ensemble, GIT_COMMIT
from disconnected.read_disconnected import (
    read_disconnected_diff,
    read_disconnected_ensemble,
)
from resample.resample import jackknife, jackknife_average

# pylint: disable=too-many-locals, consider-using-f-string

qsquare = 1 / 9  # to reproduce plots in paper


def plot_mean(mean, std, labels, args, a, show=False):
    """plots disconnected contribution without charge factor.

    Args:
        mean (list): list of np.array with length N0/2
        std (list): list of np.array with length N0/2
        labels (list): list of str
        a (float): lattice spacing
        filename (str): filename for storing plot
    """
    assert len(mean) == len(std)
    filename = f"pdfs/disconnected_{args.ensemble.name}_{'_'.join(args.type)}"
    if args.save:
        filename += f"_{args.save}"
    filename += ".pdf"

    with PdfPages(filename) as pdf:
        # plt.figure(figsize=(8, 6))
        for i, m in enumerate(mean):
            plt.errorbar(
                a[i][0] * np.arange(len(m)),
                -m / qsquare,
                std[i] / qsquare,
                fmt=".",
                label=labels[i],
                # alpha=1 if labels[i] == "hopping_symm " else 0.3,
            )

        plt.fill_between(a[0][0] * np.arange(20), -0.00025, 0.00025, color="b", alpha=0.2,
                         label="integration domain")
        plt.legend()
        plt.xlabel(r"$x_0 / fm$")
        plt.ylabel(r"$a^3 C_{VV}$")
        plt.gca().spines[['right','top']].set_visible(False)
        # plt.title(
        #     "Disconnected two-point function, {}, {} cnfgs".format(
        #         args.ensemble.name, len(args.configs))
        # )
        metadata = pdf.infodict()
        metadata["Subject"] = f"git commit: {GIT_COMMIT[:8]}, command: {' '.join(sys.argv)}"
        plt.grid(which="major", axis="y")
        pdf.savefig(bbox_inches='tight')
        if show:
            plt.show()
        plt.close()


def onepoint(corr, vol, label=""):
    """calculate one point function sum_{vec{x},k,f} < q_f j_{f,k}(x)>

    Args:
        corr (np.ndarray): shape (N0/2, ncnfg, nsrcs, ngamma)
        vol (float): spatial volume
        label (str): label for plot
    """
    nsrcs = np.arange(corr.shape[2], 0, -2)
    ncnfg = corr.shape[1]
    std = np.zeros((corr.shape[0], len(nsrcs)))
    for i, ns in enumerate(nsrcs):
        cc = np.mean(corr[..., :ns, 0], axis=2)
        mean = np.mean(cc, axis=1)
        corr_jk = np.mean(jackknife(cc, axis=1), axis=1 + 1)
        _, std[:, i] = jackknife_average(corr_jk, mean)
    plt.plot(nsrcs, ncnfg * std[10, :] ** 2 / vol / qsquare, ".", label=label)


def onepoint_plot(ensemble: Ensemble, save=False):
    """plot one point function, see Eq. (5.9) in [1]

    Args:
        ensemble (configs.directories.Ensemble): ensemble
        plot (bool): if True, show plot.

    """
    gammas = np.array([1, 2, 3])
    vol = np.prod(ensemble.volume[1:])

    src = ensemble.sources
    ensemble.sources = np.arange(20)
    corr1 = read_disconnected_ensemble(ensemble, "disc_loc_loc", gammas)
    corr1 = np.sum(corr1, axis=-1)  # sum over flavors
    onepoint(corr1, vol, label="standard")
    ensemble.sources = src
    corr2 = read_disconnected_diff(ensemble, "disc_loc_loc", gammas)
    onepoint(corr2, vol, label="split-even")

    plt.yscale("log")
    plt.xscale("log")
    plt.xlabel(r"$N_s$")
    plt.ylabel(r"$\sigma^2 \cdot (a/L)^3$")
    plt.title("Variance of vector one-point function")
    plt.legend()
    if save:
        plt.savefig(f"pdfs/disconnected_variance1pt_{ensemble.name}.pdf")
    else:
        plt.show()
    plt.close()
