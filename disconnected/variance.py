"""calculates the variance of tr[ Γ S(x|x)] as a function of the number
of sources.

Run:
python -m disconnected.variance --ensemble A380a07b324 --type hopping \
    --configs 10 50 10 --sources 1000 --suffix ""

python -m disconnected.variance --ensemble A400a00b324 \
    --type disconnected_nohopping --suffix _spliteven_f0_0_conserved \
    --suffix _hopping_conserved
"""
import sys

from uncertainties import ufloat
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from scipy.stats import sem

from config.directories import GIT_COMMIT
from resample.resample import jackknife, jackknife_average
from read.parse import disconnected_twopoint_arguments
from read.read_meson import read_data
from disconnected.generate_twopoint import read_onepoint_splitting

REFERENCE = True

def get_variance_timeslice(corr):
    """sums correlator over different flavors, averages over cnfg and
    src.

    Args:
        corr (np.ndarray): shape (ncnfg, nsrc, nfl, ngamma, N0)
            or (ncnfg, nfl, ngamma, N0)

    Returns:
        (float, float): variance, error on variance
    """
    if corr.ndim == 5:
        ncnfg, _, nfl, _, N0 = corr.shape
        corr = nfl * np.mean(corr, axis=(1,2,3))
    elif corr.ndim == 4:
        ncnfg, nfl, _, N0 = corr.shape
        corr = nfl * np.mean(corr, axis=(1,2))
    else:
        raise TypeError(f"corr must be 4- or 5-dimensional, but has shape {corr.shape}")
    corr_jk = jackknife(corr, axis=0)
    var = np.zeros((ncnfg, N0))
    for i in range(corr_jk.shape[0]):
        var[i, :] = sem(corr_jk[i,...], axis=0)**2
    var_jk, err_jk = jackknife_average(var)
    return np.mean(var_jk), np.linalg.norm(err_jk) / len(var_jk)


def create_plot(corr, label=None, color=None, reference=False):
    """creates plot for naive implementation of disconnected terms.

    Modifies global variables REFERENCE.

    Args:
        corr (np.ndarray): correlator, shape
            (ncnfg, nsrc, nfl, ngamma, N0), where first
            dimension contains jackknife samples.
        reference (bool): Create 1/Ns plot
        label (str): label for plot
    """
    global REFERENCE
    nsrc = corr.shape[1]
    if args.sources is not None:
        nsrc = np.min([nsrc, np.max(args.sources)+1])
    print("nsrc = ", nsrc)
    nelements = int(np.floor(np.log2(nsrc))) + 1
    var = np.zeros(nelements)
    err = np.zeros(nelements)
    srcs = np.array(
        list(map(int, 2**np.arange(1, 1 + np.floor(np.log2(nsrc)))))
        + [nsrc-1])
    for i in range(len(srcs)): # iterate over jackknives
        var[i], err[i] = get_variance_timeslice(corr[:,:srcs[i],...])

    if "local" in label:
        mfc = "white"
        fmt = "o"
    else:
        mfc = None
        fmt = "."

    if "spliteven" in label:
        color = "b"
    elif "naive" in label:
        color = "orange"
    elif "hopping" in label:
        color = "g"

    p = plt.errorbar(srcs, var, err, fmt=fmt, label=label, color=color,
                     mfc=mfc)

    if reference:
        if REFERENCE:
            REFERENCE = False
            plt.plot(srcs, var[0] * srcs[0] / srcs, "k--",
                     alpha=0.3, label=r"$1/N_s$")
            # plt.hlines(var[-1], srcs[-3], srcs[-1], color=p[0].get_color(),
            #    linestyles="dashed", label="gauge noise")
        else:
            plt.plot(srcs, var[0] * srcs[0] / srcs, "k--", alpha=0.3)
            # plt.hlines(var[-1], srcs[-3], srcs[-1], color=p[0].get_color(),
            #    linestyles="dashed")

    return var[-1], err[-1]

def evaluate_variance(ensemble, curr_type, suffix):
    corr_total = read_data(ensemble, curr_type, suffix)
    if "splitting" in curr_type:
        correlators = read_onepoint_splitting(args.ensemble, curr_type)
        labels = [r"split-even $m_u \rightarrow m_{ds}$",
                    r"split-even $m_{ds} \rightarrow m_c$",
                    r"split-even $m_{ds} \rightarrow m_c$",
                    r"hopping $m_c$ 4th order"]
        colors = [(0,1,0), (0,0.75,0), (0,0.75,0), (0.75,0,0)]
        correlators[1] += correlators[2]
        if curr_type == "splitting":
            for j in [0, 1, 3]:
                var, err = create_plot(correlators[j], label=labels[j],
                                        color=colors[j], reference=True)
        else:
            j = 3
            var, err = create_plot(correlators[j], label=r"hopping $m_c$ 8th order",
                                    color=(0.5,0,0), reference=True)
            print(f"Variance {labels[j]}: {ufloat(var, err)}")
        corr = read_data(ensemble, curr_type)
        var, err = get_variance_timeslice(corr)
    else:
        if "spliteven" in suffix:
            split = suffix.split("_")
            label = " ".join([split[1], split[-1]])
        else:
            label = " ".join(suffix.split("_"))
        var, err = create_plot(corr_total, label=label, reference=True)
    result = ufloat(var, err)
    print(f"Variance: {result}")
    print("----------------------------")

if __name__ == "__main__":
    args = disconnected_twopoint_arguments()
    ref = True

    filename = f"pdfs/variance_{args.ensemble.name}_{'_'.join(args.type)}_{'_'.join(args.suffix)}.pdf"
    with PdfPages(filename) as pdf:
        for i, curr_type in enumerate(args.type):
            for suffix in args.suffix:
                print(curr_type + suffix)
                evaluate_variance(args.ensemble, curr_type, suffix)

        plt.xscale("log")
        plt.yscale("log")
        plt.title(f"Variance of vector one-point function {args.ensemble.name}")
        plt.legend()
        plt.xlabel(r"$N_s$")
        plt.ylabel(r"$a^2 \sigma^2$")
        metadata = pdf.infodict()
        metadata["Subject"] = f"git commit: {GIT_COMMIT[:8]}, command: {' '.join(sys.argv)}"
        pdf.savefig()
        plt.show()
        plt.close()
