"""vary kappa and determine <psibar psi>"""
import numpy as np

from uncertainties import ufloat, ufloat_fromstr
import matplotlib.pyplot as plt

from disconnected.free import plot_exact

kappas = np.array([0.08, 0.09, 0.10, 0.105, 0.11, 0.112, 0.115, 0.11803188, 0.12, 0.12360817, 
                   0.1248, 0.1249, 0.125])
numerics = np.array([
            ufloat_fromstr("326.31+/-0.43"),
            ufloat_fromstr("365.60+/-0.48"),
            ufloat_fromstr("403.46+/-0.54"),
            ufloat_fromstr("421.54+/-0.56"),
            ufloat_fromstr("438.80+/-0.58"),
            ufloat_fromstr("445.42+/-0.59"),
            ufloat_fromstr("454.98+/-0.61"),
            ufloat_fromstr("463.65+/-0.15"), 
            ufloat_fromstr("469.63+/-0.62"),
            ufloat_fromstr("478.52+/-0.63"),
            ufloat_fromstr("481.02+/-0.64"),
            ufloat_fromstr("481.22+/-0.64"),
            ufloat_fromstr("481.42+/-0.64"),
])
exact = plot_exact(kappas, -1/3, np.array([16,8,8,8]))
ratio = exact / numerics
print("kappa: ratio")
for i in range(len(ratio)):
    print(f"{kappas[i]:.3f}:\t {ratio[i].n:.4f}")

plt.plot(kappas, [n.n for n in numerics], '.', label="numerics")
plt.plot(kappas, exact, '.', label="exact")
# plt.plot(kappas, [400*r.n for r in ratio], '.', label="100 x ratio")
plt.xlabel("kappa")
plt.ylabel(r"$\sum_{spatial} \langle \overline{\psi}(x) \psi(x) \rangle$")
plt.legend()
plt.savefig("kappas.pdf")
plt.show()