"""Generates charm contribution for splitting estimator."""
import numpy as np

from read.parse import arguments
from disconnected.generate_twopoint import (read_onepoint_splitting,
                                            write_onepoint_splitting)

if __name__ == "__main__":
    args = arguments()
    ensemble = args.ensemble
    curr_type = args.type[0]
    one = read_onepoint_splitting(ensemble, curr_type)
    # add extra flavor dimension
    for i in range(len(one)):
        one[i] = np.pad(one[i], ((0,0), (0,0), (0,1), (0,0), (0,0)))
    one[-1][:,:,-1,...] = one[-1][:,:,0,...]
    write_onepoint_splitting(ensemble, curr_type + "_charm_generated", one)
