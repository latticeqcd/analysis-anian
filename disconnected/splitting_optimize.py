"""analyzes how many sources to use for splitting steps."""
import numpy as np
from scipy.stats import sem

from read.parse import arguments
from disconnected.generate_twopoint import generate_twopoint_splitting
from disconnected.gm2 import gm2_disconnected
from kernel.kernel import get_kernel

eps = 1e-15

parts = ["_spliteven_f0_0.dat", "_spliteven_f0_1.dat",
            "_spliteven_f1_1.dat",
            "_hopping_us.dat"]

# parts = ["_mass_0_us.dat", "_mass_0_sc.dat", "_mass_1_sc.dat",
#          "_hopping_us.dat"]


def find_optimal_sources(Tactual, Ts, sigmas):
    """Returns number of sources for each step such that
    each step has similar variance."""
    if Tactual < 0:
        raise ArithmeticError("Actual time must be positive")
    alpha = Tactual / sum(Ts * sigmas**2)
    Ns = alpha * sigmas**2
    return Ns, len(Ts) / alpha


def print_log(onepoint_cnfgs, nsrcs, log=False):
    """Prints some info about variance"""
    one = np.array(onepoint_cnfgs)
    one = np.sum(one, axis=2) # sum flavors
    one = np.mean(one, axis=2) # average over gammas
    one = np.mean(one, axis=-1) # average over time
    std = sem(one, axis=0) # standard deviation over configs
    total_std = sem(np.sum(one, axis=1), axis=0) # total deviation over configs
    if log:
        print("standard deviation over configs: ")
        print("step-by-step: ", std**2)
        print("sources: ", nsrcs)
        print("variance per source: ", nsrcs*std**2)
        print("total: ", total_std**2)
    return np.sqrt(nsrcs) * std


def total_time(nsrc, Ts, Thop):
    """Returns time needed for splitting"""
    return Thop + sum(nsrc*Ts)

if __name__ == "__main__":
    args = arguments()
    for curr_type in args.type:
        # nsrc = [5, 8, 8, 16] # times 20
        # nsrc = [4, 20, 20, 130] # times 5
        # nsrc = [10, 50, 50, 650]
        nsrc = [20, 200, 200, 1000]
        twos = generate_twopoint_splitting(args.ensemble, curr_type, nsrc, write=False)
        N0half = twos.shape[0]
        kernel0 = get_kernel(np.arange(N0half), args.ensemble.m_mu(),
                         args.ensemble.name)
        _, _, result = gm2_disconnected(twos, kernel0)
        print(f"g-2: {result:.3e}")
        Ts = np.array([49.4, 27.9, 32.7, 16.8])
        Thop = 199
        print("total time: ", total_time(nsrc, Ts, Thop), " s")

        # sigmas = np.array([sigmas[0], np.linalg.norm(sigmas[1:3])/2,
        #                    np.linalg.norm(sigmas[1:3])/2, sigmas[3]])
        # Ts = np.array([Ts[0], np.sum(Ts[1:3])/2, np.sum(Ts[1:3])/2, Ts[3]])
        # Tactual = T0 - Thop
        # Ns, var = find_optimal_sources(Tactual, Ts, sigmas)
        # print("constant sigma: ", Ns)
        # print("constant time: ", Tactual / (Ts * len(Ts)))
        # print("Expected variances: ", var, sum(sigmas**2*Ts)*len(Ts)/(T0-Thop))
