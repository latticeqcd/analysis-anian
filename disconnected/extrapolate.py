"""extrapolates the disconnected contribution.

run from git root:
    python -m disconnected.extrapolate --ensemble A380a07b324 \
        --type disc_loc_loc --type-conn ju_0_loc_loc

Reference:

[1] Vera Gülpers, Hadronic Correlation Functions with quark-disconnected
    Contributions in Lattice QCD, http://doi.org/10.25358/openscience-1255

"""
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import sem

from disconnected.disconnected import load_twopoint_single
from read.parse import extrapolate_arguments
from read.read_meson import get_correlator_ensemble, get_correlator_ensemble_new

def prefactor(ensemble, curr_type):
    """determines the charge factor for the connected two-point function.

    Args:
        ensemble (ensemble): ensemble
        curr_type (str): directory, e.g. ju_0_loc_loc
    """
    if ensemble.name == "A380a07b324":
        if "ju_0_loc_loc" in curr_type:
            up = (2/3)**2
            return up
        if "ju_1_loc_loc" in curr_type:
            down = (-1/3)**2
            strange = (-1/3)**2
            return down + strange
        if "ju_3_loc_loc" in curr_type:
            charm = (2/3)**2
            return charm
        else:
            raise TypeError(f"{curr_type} not implemented")
    elif ensemble.name == "A400a00b324":
        if "connected" in curr_type:
            # charm
            return (2/3)**2
        else:
            raise TypeError(f"{curr_type} not implemented")
    else:
        raise TypeError(f"{ensemble.name} not implemented")

def load_connected(ensemble, type_conn, suffix=""):
    """loads connected contribution

    Args:
        ensemble: ensemble
        type_conn (list): list of strings with directories,
            e.g. ju_0_loc_loc

    Returns:
        np.ndarray, shape (N0/2, ncnfg)
    """
    assert type_conn is not None
    connected = 0
    for i in range(len(type_conn)):
        try:
            tmp = get_correlator_ensemble(ensemble, type_conn[i], suffix=suffix)
        except:
            tmp = get_correlator_ensemble_new(ensemble, type_conn[i], suffix=suffix)
        connected += prefactor(ensemble, type_conn[i]) * tmp
    return connected


if __name__ == "__main__":
    args = extrapolate_arguments()
    ensemble = args.ensemble

    i = 0
    disconnected = load_twopoint_single(ensemble, args.type[i])
    disconnected *= -1
    connected = load_connected(ensemble, args.type_conn)
    mean_disc = np.mean(disconnected, axis=1)

    plt.figure(figsize=(8,6))
    plt.errorbar(np.arange(disconnected.shape[0]),
                 np.mean(disconnected, axis=1),
                 sem(disconnected, axis=1), fmt=".", label="- disconnected")
    plt.errorbar(np.arange(connected.shape[0]),
                 np.mean(connected, axis=1),
                 sem(connected, axis=1), fmt="k.", label="connected")
    # plt.errorbar(np.arange(connected.shape[0]),
                #  -np.mean(connected, axis=1),
                #  sem(connected, axis=1), fmt="k.")
    plt.hlines(0, 0, disconnected.shape[0], colors="k", linestyles="dashed")
    # plt.ylim([1.1*min(mean_disc), 1.1*max(mean_disc)])
    plt.ylim([-4e-5, 4e-5])
    plt.legend()
    plt.xlabel(r"$x_0 / a$")
    plt.ylabel(r"$a^3 C_{VV}$")
    plt.title(f"Two-point function {ensemble.name}")
    plt.savefig(f"pdfs/disconnected_{ensemble.name}_bound.pdf")
    plt.show()
