"""analyze splitting

Run:
python -m disconnected.splitting --ensemble A5d --type disc_loc_loc --configs 1 51
"""
import sys

import numpy as np
from uncertainties import ufloat
from scipy.stats import sem

from config.directories import GIT_COMMIT
from disconnected.generate_twopoint import read_onepoint_splitting
from disconnected.variance import get_variance_timeslice
from read.parse import arguments


def variance_flavor(one):
    """Analyzes variance of one-point per flavor

    Args:
        one (np.ndarray): shape (ncnfg, nsrc, nfl, ngamma, N0)

    Returns:
        variance: length 2
    """
    # shape (ncnfg, nsrc, nfl, ngamma, N0)
    std = np.zeros(one.shape[2])
    for i in range(one.shape[2]):
        one_i = np.mean(one[:,:,i,:,:], axis=(1,2,3)) # average nsrc, ngamma, N0
        std[i] = sem(one_i, axis=0)
    return std**2

if __name__ == "__main__":
    args = arguments()

    print("Git commit: ", GIT_COMMIT)
    print("command: ", " ".join(sys.argv))
    for i, curr_type in enumerate(args.type):
        corr = read_onepoint_splitting(args.ensemble, curr_type)
        print("Variances one-point for different steps (last is hopping, others are split-even)")
        for i in range(len(corr)-1):
            var, err = get_variance_timeslice(corr[i])
            print(f"step {i}: {ufloat(var, err):.1e}")
