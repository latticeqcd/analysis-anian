"""plots the variance for different charges of the same ensemble

run from git root:
    python -m disconnected.disconnected_ib --ensemble A380a07b324
        --type

Reference:

[1] Vera Gülpers, Hadronic Correlation Functions with quark-disconnected
    Contributions in Lattice QCD, http://doi.org/10.25358/openscience-1255

"""
# pylint: disable=import-error,no-name-in-module

import os
import sys
from string import Template

from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import numpy as np

from disconnected.convolute import convolute_ensemble
from config.directories import GIT_COMMIT
from disconnected.plots import onepoint_plot, plot_mean
from disconnected.read_disconnected import (read_disconnected_diff,
                                            read_disconnected_ensemble)
from read.parse import arguments

from read.read_meson import fold_correlator, read_meson_file
from resample.resample import jackknife, jackknife_average

def plot_variance(ensemble, file, sources, store, save=False, **kwargs):
    """plots variance of estimators against number of sources used.

    The variances are assumed to be stored in the variable `file` after
    substituting the placeholders.

    Args:
        ensemble (config.directories.Ensemble): ensemble used
        file (string.Template): string template with variables ${base},
            and ${i}, where i is the source index, e.g. "${base}_ncnfg5.${i}"
        sources (np.array): number of sources (x-axis in plot)
    """
    x0 = 10  # plot at this time
    ncnfg = len(ensemble.configs)
    del kwargs["base"]
    base = os.path.join(store, ensemble.name)

    sources1 = sources[np.where(sources <= 20)[0]]
    std1 = np.zeros((len(sources1), int(ensemble.volume[0] / 2)))
    for i, nsrc in enumerate(sources1):
        kwargs["nsrc"] = nsrc
        std1[i, :] = np.loadtxt(
            file.substitute(base=base + "_naive", i="std", **kwargs)
        )

    std2 = np.zeros((len(sources), int(ensemble.volume[0] / 2)))
    for i, nsrc in enumerate(sources):
        kwargs["nsrc"] = nsrc
        std2[i, :] = np.loadtxt(
            file.substitute(base=base + "_split_even", i="std", **kwargs)
        )

    with PdfPages(f"pdfs/disconnected_variance2pt_{ensemble.name}.pdf") as pdf:
        plt.plot(
            sources1, ncnfg * std1[:, x0] ** 2, ".", label="standard"
        )
        plt.plot(
            sources, ncnfg * std2[:, x0] ** 2, ".", label="split-even"
        )
        plt.xscale("log")
        plt.yscale("log")
        plt.title(r"Variance of vector-vector two-point function")
        plt.legend()
        plt.xlabel(r"$N_s$")
        plt.ylabel(r"$\sigma^2 \cdot a^6$")
        metadata = pdf.infodict()
        metadata["Subject"] = f"git commit: {GIT_COMMIT[:8]}, command: {' '.join(sys.argv)}"
        if save:
            pdf.savefig()
        else:
            plt.show()
        plt.close()


def convolute_save(ensemble, corr, file, base):
    """convolutes the data to obtain the disconnected contribution.

    Args:
        ensemble (config.directories.Ensemble): ensemble
        corr (np.ndarray): shape (N0/2, ncnfg, nsrc, ngammas)
        file (string.Template): for storing file
        base (str): base file path

    Returns:
        mean: np.array with length N0/2
        std: np.array with length N0/2
        info: dict with storage info
    """
    info = {
        "base": base,
        "ncnfg": corr.shape[1],
        "nsrc": corr.shape[2],
        "ngammas": corr.shape[3],
    }
    meanfile = file.substitute(i="mean", **info)
    stdfile = file.substitute(i="std", **info)
    try:
        meanjk = np.loadtxt(meanfile)
        stdjk = np.loadtxt(stdfile)
        raise FileNotFoundError
    except FileNotFoundError:
        conv = convolute_ensemble(corr, np.prod(ensemble.physvolume))

        # gives the same as mean, std
        mean = np.mean(conv, axis=1)
        # std = np.std(conv, axis=1) / np.sqrt(corr.shape[1]-1)
        if conv.shape[1] > 1:  # if there is more than one configuration
            conv_jk = np.mean(jackknife(conv, axis=1), axis=1 + 1)
            meanjk, stdjk = jackknife_average(conv_jk, mean)
        else:
            meanjk = mean
            stdjk = np.zeros_like(meanjk)

        np.savetxt(file.substitute(i="mean", **info), meanjk)
        np.savetxt(file.substitute(i="std", **info), stdjk)
    return meanjk, stdjk, info


def twopoint(
    args, file, store, save=False, plot=False
):  # pylint: disable=too-many-locals
    """calculates naive and split-even estimator of the two-point
    function for a given ensemble.

    Args:
        args (argparse.Namespace): command line arguments.
        ensemble (config.directories.Ensemble): ensemble.
        file (string.Template): for storing file
        store (str): place to store mean, std
        naive (bool): analyze naive estimator
        plot (bool): if True, plot disconnected contribution.
    """
    gammas = np.array([1, 2, 3])
    ensemble = args.ensemble
    base = os.path.join(store, ensemble.name)

    runname = ensemble.runname
    means, stds = [], []

    # ensemble.runname = runname + "fake"
    # corr = read_disconnected_diff(ensemble, args.type, gammas)
    # out = convolute_save(ensemble, corr, file, base + "_fake")
    # means += [1e3 * out[0]]
    # stds += [1e3 * out[1]]

    # ensemble.runname = runname + "ibe"
    corr = read_disconnected_ensemble(ensemble, args.type, gammas)
    for corr_fl in [corr, corr[...,0:1], corr[...,1:]]:
        corr_sum = np.sum(corr_fl, axis=-1)  # sum over flavors
        print("aa: ", end="")
        if np.linalg.norm(corr_sum - corr_fl[...,0]) < 1e-10:
            print("single")
        # corr = read_disconnected_diff(ensemble, args.type, gammas)
        # out = convolute_save(ensemble, corr_sum, file, base + "_ibe")
        corr_sum = np.mean(corr_sum, axis=(2,3))
        means += [np.mean(corr_sum, axis=1)]
        stds += [np.std(corr_sum, axis=1)/np.sqrt(corr.shape[1]-1)]
    # ensemble.runname = runname
    for ss in stds:
        print(ss[:10])
    if plot:
        plot_mean(means, stds, ["sum", "fl 1", "fl 2"], args, show=not save)


def load_twopoint(args):
    """loads twopoint function generated by disconnected.c
    and return variance at a single point"""
    runname = args.ensemble.runname
    gg = np.zeros((args.ensemble.volume[0], len(args.ensemble.configs)))
    base = os.path.join(args.ensemble.dir, args.type, runname)
    ncnfg = len(args.ensemble.configs)
    means = []
    std = []
    x0 = 3
    for type in ["fake", "ibe"]:
        args.ensemble.runname = runname + type
        for i, cnfg in enumerate(args.ensemble.configs):
            gg[:, i] = np.real(read_meson_file(base + f"{type}n{cnfg}_twopoint.dat"))
        corr = fold_correlator(gg)
        factor = 1e4
        if type == "fake":
            corr *= factor
        means += [np.mean(corr, axis=1)]
        std += [np.std(corr, axis=1) / np.sqrt(ncnfg)]
    plot_mean(means, std, [f"{int(factor)} * mass diff", "ibe naive diff"], args)
    return (std[0][x0]/factor, std[1][x0])


def check_c_convolution(args):
    """not needed any more, was used to check twopoint convolution of C."""
    runname = args.ensemble.runname
    base = os.path.join(args.ensemble.dir, args.type, runname)
    gammas = np.array([1, 2, 3])
    gg = np.zeros((args.ensemble.volume[0], len(args.ensemble.configs)))
    types = ["fake"]
    saved = {}
    convoluted = {}
    for type in types:
        for i, cnfg in enumerate(args.ensemble.configs):
            gg[:, i] = np.real(read_meson_file(base + f"{type}n{cnfg}_twopoint.dat"))
        saved[type] = fold_correlator(np.mean(gg, axis=1))
        args.ensemble.runname = runname + type
        if type == "ibe":
            corr = read_disconnected_ensemble(args.ensemble, args.type, gammas)
            corr = np.sum(corr, axis=-1)
            # corr = read_disconnected_diff(args.ensemble, args.type, gammas)
        elif type == "fake":
            corr = read_disconnected_diff(args.ensemble, args.type, gammas)
        else:
            raise Exception(f"Unknown type {type}")
        print(corr.shape)
        isrc = 10
        igamma = 1
        i = 3
        print("first: ", corr[i, 0, isrc, igamma])
        convoluted[type], std, info = convolute_save(
            args.ensemble, corr, file, base + "_whatever"
        )

        print(saved[type] / convoluted[type])
        print(np.linalg.norm(saved[type] - convoluted[type]))
        print()


if __name__ == "__main__":
    JK_STORE = os.path.join(os.path.dirname(os.path.abspath(__file__)), "jackknife100")
    VARIANCE = False
    args = arguments()
    args.save = None
    plotter = False
    file = Template("${base}_ncnfg${ncnfg}_nsrc${nsrc}_ngamma${ngammas}.${i}")

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']

    twopoint(args, file, JK_STORE, save=False, plot=True)

    if plotter:
        stds = np.zeros((2,4))
        L = np.array([8, 16, 24, 32])
        # i = 0
        # stds[:,i] = load_twopoint(args)
        # i += 1
        print(stds)
        plt.plot(L, stds[0,:], 'o', color=colors[0], label="mass diff")
        plt.plot(L, stds[1,:], 'o', color=colors[1], label="ibe")
        plt.plot(L, stds[0,0]*L[0]**2/L**2, '--', color=colors[0], label=r"$L^{-2}$")
        plt.plot(L, stds[1,0]*(L[0]/L)**0.5, '--', color=colors[1], label=r"$L^{-0.5}$")
        plt.legend()
        plt.xscale("log")
        plt.yscale("log")
        plt.xlabel("L/a")
        plt.ylabel(r'$\sigma(x_0/a=3)$')
        plt.title("Variance of two-point function for QED ensembles")
        # plt.savefig("pdfs/variance_qed.pdf")
        plt.show()
        # check_c_convolution(args)

    # if VARIANCE:
    #     maxsrc = args.ensemble.sources[-1]
    #     sources = np.arange(2, maxsrc + 1, 2)
    #     for src in sources:
    #         args.ensemble.sources = np.arange(src)
    #         _, info = twopoint(args.ensemble, file, JK_STORE, plot=False)
    #     args.ensemble.sources = np.arange(maxsrc)
    #     onepoint_plot(args.ensemble, args.save)
    #     plot_variance(args.ensemble, file, sources, JK_STORE, args.save, **info)
