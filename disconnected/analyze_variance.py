"""analyzes the variance and cost of estimators

run from git root:
    python -m disconnected.disconnected --ensemble A5d --type hopping

Reference:

[1] Vera Gülpers, Hadronic Correlation Functions with quark-disconnected
    Contributions in Lattice QCD, http://doi.org/10.25358/openscience-1255

"""
# pylint: disable=import-error,no-name-in-module
import os
import subprocess

import numpy as np
from uncertainties import ufloat

from read.parse import arguments
from read.read_meson import read_array_file
from disconnected.variance import get_variance_timeslice


def load_onepoint(ensemble, curr_type, suffix=""):
    """Loads twopoint function for a single current type

    Args:
        ensemble (ensemble): ensemble
        curr_type (str): current type
        suffix (str): e.g. _splitting_f0_0

    Returns:
        np.ndarray: shape (ncnfg, nsrc, nfl, ndirac, N0)
    """
    base = os.path.join(ensemble.dir, curr_type, ensemble.runname)
    gg = []
    for cnfg in ensemble.configs:
        gg += [np.real(read_array_file(base + f"n{cnfg}_onepoint{suffix}.dat"))]
    return np.array(gg)

def find_time(ensemble, curr_type):
    """Finds time needed to process configuration.

    Args:
        ensemble (ensemble): ensemble
        curr_type (str): type of estimator

    Returns:
        ufloat: mean and standard deviation of time needed.
    """
    base = os.path.join(ensemble.dir, curr_type)
    cmd = 'grep "fully processed in" *log'
    cmd += " | awk '{ print  $7 }' "
    cwd = os.getcwd()
    os.chdir(base)
    out = subprocess.check_output(cmd, shell=True).decode()
    res = [float(el) for el in out.split("\n") if el != '']
    os.chdir(cwd)
    return ufloat(np.mean(res)/3600, np.std(res)/3600)



if __name__ == "__main__":
    args = arguments()
    for curr_type in args.type:
        one = load_onepoint(args.ensemble, curr_type)
        std, err_std = get_variance_timeslice(one)
        result = ufloat(std, err_std)
        time = find_time(args.ensemble, curr_type)
        print(f"{curr_type:<10}", f"{result:.4f}", "\t", time, "h",
              "\t", result*time*100)
