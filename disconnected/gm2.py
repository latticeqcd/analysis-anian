"""functions related to evaluated gm2. run with
    python -m disconnected.gm2 --ensemble A380a07b324 --type splitting_4
    [--cutoff 20 --mass 0.265 --type-conn ju_0_loc_loc
    --type-conn ju_1_loc_loc --type-conn ju_3_loc_loc]
"""
# pylint: disable=unnecessary-lambda-assignment,import-error,no-name-in-module

import matplotlib.pyplot as plt
import numpy as np
import scipy
from uncertainties import ufloat

from disconnected.disconnected import load_twopoint_single
from kernel.kernel import LOKernel, window, window_parms
from read.parse import disconnected_arguments
from resample.resample import jackknife, jackknife_average
from disconnected.extrapolate import load_connected

def fix_disconnected(disconnected, iso, mass, cutoff):
    """Replaces disconnected with isoscalar disconnected contribution
    for t > cutoff.

    Args:
        disconnected (np.ndarray): disconnected contribution, shape
        (N0/2, ncnfg)
        iso (np.ndarray): isoscalar connected contribution, shape
        (N0/2, ncnfg)
        mass (float): mass of dominating particle in the long time limit
        cutoff (int): cutoff time
    """
    times = np.arange(disconnected.shape[0]-cutoff)
    print(cutoff, iso[0].shape)
    disconnected[cutoff:,...] = -iso[cutoff,...] * np.exp(-mass*times[:,np.newaxis])
    return disconnected


def gm2_disconnected(conv, kernel0, window_vec, cutoff=20):
    """calculates g-2 for a disconnected two-point function

    Args:
        conv (np.ndarray): convoluted two-point function, shape (N0/2, ncnfg)
        kernel0 (np.array): kernel function, length N0/2

    Returns:
        _type_: _description_
    """
    mean = np.mean(conv, axis=1)
    conv_jk = jackknife(conv, axis=1)
    integral = np.zeros(conv_jk.shape[0])
    for i in range(conv_jk.shape[0]):
        meanjk, _ = jackknife_average(conv_jk[i, ...], mean, axis=1)
        # trapezoid rule: h/2 * [f(x) + f(x+h)]
        integrand = (window_vec * kernel0 * meanjk)[:cutoff]
        integral[i] = 0.5 * integrand[0] + np.sum(integrand[1:-1]) + 0.5 * integrand[-1]
    mean, std = jackknife_average(np.mean(conv_jk, axis=1 + 1), mean)
    result = ufloat(np.mean(integral), scipy.stats.sem(integral))
    return mean, std, result


def execute(args, plot=True, color=""):
    """performs an exponential fit, integrates data and plots it.

    Args:
        args (argparse.Namespace): command line arguments
        color (str): color for plot
    """
    # correlators = load_twopoint(args.ensemble, args.type, quark_types)
    N0half = int(args.ensemble.volume[0]/2)
    kernel0 = LOKernel.get_kernel(np.arange(N0half), args.ensemble.m_mu(),
                         args.ensemble.name)
    results = []
    min_val, max_val = np.inf, -np.inf

    if args.window in window_parms.keys():
        a = args.ensemble.a[0]
        w_parms = window_parms[args.window](a)
        window_vec = window(*w_parms)(np.arange(N0half))
    else:
        window_vec = np.ones(N0half)

    if args.mass is not None:
        iso = load_connected(args.ensemble, args.type_conn, suffix=args.suffix_conn)

    for k, curr_type in enumerate(args.type):
        correlator = load_twopoint_single(args.ensemble, curr_type, args.suffix_disc)
        mean, std, res = gm2_disconnected(correlator, kernel0, window_vec, args.cutoff)
        if args.mass is not None:
            correlator_fixed = fix_disconnected(correlator, iso, args.mass, args.cutoff)
            mean_fixed, std_fixed, res = gm2_disconnected(correlator_fixed, kernel0, window_vec)
        results += [res]

        if plot:
            min_val = min(np.min(window_vec * mean*kernel0), min_val)
            max_val = max(np.max(window_vec * mean*kernel0), max_val)
            p = plt.errorbar(np.arange(N0half), window_vec * kernel0 * mean,
                             window_vec * kernel0 * std, fmt=f"{color}.",
                             label=args.type[k])
            if args.mass is not None:
                plt.errorbar(np.arange(args.cutoff, N0half),
                    (window_vec * kernel0 * mean_fixed)[args.cutoff:],
                    (window_vec * kernel0 * std_fixed)[args.cutoff:],
                    color=p[0].get_color(), fmt=".", alpha=0.5,
                    label=f"{args.type[k]} bounded")
    if plot:
        plt.vlines([args.cutoff], ymin=min_val, ymax=max_val, colors="k",
               linestyles="--", label="cutoff")
    return results


if __name__ == "__main__":

    args = disconnected_arguments()

    res_qxd = execute(args)
    for i in range(len(args.type)):
        print(f"{args.type[i]}: {res_qxd[i]:.3e}")

    plt.xlabel(r"$x_0/a$")
    plt.ylabel(r"$(\alpha/\pi)^2 G(x_0) \cdot \tilde{K}(x_0;m_\mu) \cdot a$")
    plt.title(f"disconnected {args.ensemble.name} {len(args.configs)} cnfgs")
    plt.grid()
    plt.legend()
    plt.savefig(f"pdfs/integrand_disconnected_{args.ensemble.name}_{'_'.join(args.type)}.pdf")
    plt.show()
