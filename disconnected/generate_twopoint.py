"""generates two-point function from one-point function.

Usage:
python -m disconnected.generate_twopoint --ensemble A380a07b324 \
    --type splitting [--configs 10 50 10]
"""
import os

import numpy as np

from disconnected.convolute import convolute
from read.parse import arguments
from read.read_meson import read_array_file, read_meson_file, fold_correlator, write_array_file, read_data
from read.read_utils import write_double, write_int

eps = 1e-15

# PARTS = ["_mass_0_us.dat", "_mass_0_sc.dat", "_mass_1_sc.dat",
#          "_hopping_us.dat"]

# PARTS = ["_spliteven_f0_0.dat", "_spliteven_f0_1.dat",
#            "_spliteven_f1_1.dat", "_hopping_us.dat"]

PARTS = ["_spliteven_f0_0.dat", "_spliteven_f0_1.dat",
        "_spliteven_f1_1.dat", "_hopping.dat"]


def write_onepoint_splitting(ensemble, curr_type, ones):
    """Writes one-point function for splitting estimator.

    Args:
        ensemble (ensemble): Ensemble
        curr_type (str): current type, e.g. splitting
        ones: list, has shapes ones[4][ncnfg, nsrc, nfl, ngamma, N0]
    """
    ncnfg, _, nfl, ngamma, N0 = ones[0].shape
    total_one = np.zeros((ncnfg, nfl, ngamma, N0), dtype=complex)
    for i in range(len(PARTS)):
        total_one += np.mean(ones[i], axis=1)

    for j, cnfg in enumerate(ensemble.configs):
        for i in range(len(PARTS)):
            base = os.path.join(ensemble.dir, curr_type, f"{ensemble.runname}n{cnfg}_")
            write_array_file(base + "onepoint" + PARTS[i], ones[i][j])
        write_array_file(base + "onepoint.dat", total_one[j])


def read_onepoint_splitting(ensemble, curr_type):
    """Reads one-point function for splitting estimator

    Args:
        ensemble (ensemble): Ensemble
        curr_type (str): current type, e.g. splitting

    Returns:
        list: has shape ones[4][ncnfg, nsrc, nfl, ngamma, N0]
    """
    ones = []
    for part in PARTS:
        onepoints = []
        for cnfg in ensemble.configs:
            base = os.path.join(ensemble.dir, curr_type, f"{ensemble.runname}n{cnfg}_")
            onepoints += [read_array_file(base + "onepoint" + part)]
        ones += [np.array(onepoints)]
    return ones


def read_twopoint_splitting(ensemble, curr_type, fold=False):
    """Reads one-point function for splitting estimator

    Args:
        ensemble (ensemble): Ensemble
        curr_type (str): current type, e.g. splitting
        fold (bool): If True, fold two-point function

    Returns:
        list: has shape twos[4, ncnfg, N0] if fold == False
                        twos[4, ncnfg, N0/2] if fold == True
    """
    PARTS2 = [*PARTS, "_offdiagonal.dat"]
    twos = []
    for part in PARTS2:
        gg = []
        for cnfg in ensemble.configs:
            base = os.path.join(ensemble.dir, curr_type,
                                f"{ensemble.runname}n{cnfg}_")
            tmp = np.real(read_meson_file(base + f"twopoint{part}"))
            if fold:
                gg += [fold_correlator(tmp)]
            else:
                gg += [tmp]
        twos += [np.array(gg)]
    return np.array(twos)


def create_twopoint(onepoints, volume, fold=False):
    """creates a two-point function for splitting.

    Args:
        onepoints (list): has shape onepoints[4][ncnfg, nsrc, nfl, ngamma, N0]
        length 4, each element has shape (nsrc, nfl, ngamma, N0)
        volume (float): volume of ensemble
        fold (bool): if True, folds correlator.

    Returns:
        list: shape two_cnfgs[5][ncfng, N0]
    """
    factor = 12  # PARTS * (PARTS - 1) = 4 * 3
    ncnfg = onepoints[0].shape[0]
    two = []
    for j in range(4):
        two_j = []
        for ic in range(ncnfg):
            two_j += [twopoint_wrapper(onepoints[j][ic], volume, fold=fold)]
        two += [np.array(two_j)]

    # cross-terms
    mean_one = np.array([np.mean(one, axis=1) for one in onepoints])
    two_j = []
    for ic in range(ncnfg):
        two_j += [twopoint_wrapper(mean_one[:,ic,...], volume/factor, fold=fold)]
    two += [np.array(two_j)]

    return two


def twopoint_wrapper(onepoint, volume, fold=False):
    """Wrapper for convolute and new data-format.

    Args:
        onepoint (np.ndarray): shape (nsrc, nfl, ngamma, N0)
        volume (float): volume of ensemble

    Returns:
        np.array: length N0
    """
    tmp = np.transpose(np.sum(onepoint, axis=1), (2, 0, 1))
    return convolute(np.real(tmp), volume, fold=fold)


def write_twopoint(twopoint, base, volume):
    """writes total two-point function in binary format.

    Args:
        twopoint (np.array): length N0
        base (str): directory and base name
        volume (np.array): length 4, [N0, N1, N2, N3]
    """
    storage_two = np.zeros((*twopoint.shape, 2))
    storage_two[...,0] = twopoint
    filename = base + "twopoint.dat"
    with open(filename, "wb") as f:
        write_int(f, volume)
        write_double(f, np.ravel(storage_two))
    print(f"written {filename}", flush=True)


def strip_one(ones, nsrc=None):
    """Strips one-point function to have only `nsrc`.

    Args:
        ones (list): ones[4][ncnfg, nsrc,nfl,ngamma,N0]
        nsrc (list): length 4

    Returns:
        list: ones_stripped[4][ncnfg, nsrc_stripped,nfl,ngamma,N0]
    """
    if nsrc is None:
        nsrc = len(ones) * [None]
    ones_stripped = []
    for i in range(len(ones)):
        ones_stripped += [ones[i][:,:nsrc[i],...]]
    return ones_stripped


def generate_twopoint_splitting(ensemble, curr_type, nsrc=None, write=False):
    """generates the two-point function from one-point function
    for splitting estimator

    Args:
        self (ensemble): ensemble
        curr_type (str): current type
    """
    ones = read_onepoint_splitting(ensemble, curr_type)
    if nsrc is not None:
        ones_stripped = strip_one(ones, nsrc)
    else:
        ones_stripped = ones
    for i, cnfg in enumerate(ensemble.configs):
        ones_cnfg = [ones[i:i+1,...] for ones in ones_stripped]
        twos_gen = create_twopoint(ones_cnfg, ensemble.phys_vol(), fold=not write)
        twos = np.sum(np.array(twos_gen), axis=0) # sum contributions
        twos = np.transpose(twos) # shape (N0, ncnfg)
        write_twopoint(twos[:,0], os.path.join(ensemble.dir, curr_type,
                       ensemble.runname + f"n{cnfg}_"), ensemble.volume)


if __name__ == "__main__":
    import time
    args = arguments()
    if args.ensemble.name == "qxd0":
        args.ensemble.volume = [4, 4, 4, 4]
    start = time.perf_counter()
    nsrc = np.array([20, 200, 200, 100])

    my_ones = []
    for curr_type in args.type:
        ones = read_onepoint_splitting(args.ensemble, curr_type)
        ones[0] = ones[0][:,:nsrc[0],...]
        ones[1] = ones[1][:,:nsrc[1],...]
        ones[2] = ones[2][:,:nsrc[2],...]
        ones[3] = ones[3][:,:nsrc[3],...]

        # write_onepoint_splitting(args.ensemble, curr_type, ones)
        generate_twopoint_splitting(args.ensemble, curr_type, nsrc=nsrc, write=True)
    end = time.perf_counter()
    print(f"Time: {end-start} s")
