"""calculates the disconnected contribution to the electromagnetic
two-point function.

Compare Section 7 in [1] and Section 3.6 in [2].

In the documentation, we use the following abbreviations:

    N0      time extent of lattice
    ncnfg   number of configurations
    nsrc    number of noise sources (per configuration)
    ngamma  number of Gamma matrices (3 for electromagnetic current)

Reference:

[1]     Giusti, Harris, Nada, Schaefer: Frequency-splitting estimators
        of single-propagator traces, https://arxiv.org/abs/1903.10447
[2]     Altherr, Gruber, Tavella: Notes on Overleaf:
        https://www.overleaf.com/project/619bb1ca30cf5c3ab62711d0
"""
from itertools import product

import numpy as np

from read.read_meson import fold_correlator

eps = 1e-10


def convolute_ensemble(one, vol):
    """convolutes one-point function in an off-diagonal way
    for each configuration.

    compare Eq. (7.1) in [1]

    Args:
        one (np.ndarray): real part of one-point function,
            shape (N0, ncnfg, nsrc, ngamma)
        vol (float): volume of lattice

    Returns:
        (np.ndarray): shape (N0/2, ncnfg)
    """
    T, ncnfg = one.shape[:2]
    G = np.zeros((int(T / 2), ncnfg), dtype=one.dtype)
    for i in range(ncnfg):
        G[:, i] = convolute(one[:, i, ...], vol)
    return G


def convolute_naive(one, vol):
    """convolutes one-point function in an off-diagonal way. very slow

    compare Eq. (7.1) in [1]

    Args:
        one (np.ndarray): real part of one-point function,
            shape (N0, nsrc, ngamma)
        vol (float): volume of lattice
    """
    # T: time extent
    T, nsrc, ngamma = one.shape
    G = np.zeros(T, dtype=one.dtype)
    for t, y0, i, j in product(*map(range, (T, T, nsrc, nsrc))):
        if i != j:
            for ig in range(ngamma):
                G[t] += one[(t + y0) % T, i, ig] * one[y0, j, ig]
    G /= (-1) * nsrc * (nsrc - 1) * vol * ngamma
    assert np.linalg.norm(np.imag(G)) < eps
    return fold_correlator(np.real(G))


def convolute_std(one, vol, return_std=False):
    """convolutes one-point function in an off-diagonal way and returns
    std.

    compare Eq. (7.1) in [1]

    Args:
        one (np.ndarray): shape (N0, ncnfg, nsrc, ngamma)
        vol (float): volume of lattice
    """
    # T: time extent
    T, ncnfg, nsrc, ngamma = one.shape
    G = np.zeros((T, ncnfg), dtype=one.dtype)
    for t in range(T):
        for y0 in range(T):
            for ig in range(ngamma):
                for ic in range(ncnfg):
                    mat = np.outer(
                        one[(t + y0) % T, ic, :, ig],
                        one[y0, ic, :, ig],
                    )
                    G[t, ic] += np.sum(mat) - np.sum(np.diag(mat))
    G /= (-1.0) * nsrc * (nsrc - 1) * vol * ngamma
    assert np.linalg.norm(np.imag(G)) < eps
    G = fold_correlator(np.real(G))
    if return_std:
        return np.mean(G, axis=1), np.std(G, axis=1) / np.sqrt(ncnfg)
    return np.mean(G, axis=1)


def convolute_medium(one, vol):
    """convolutes one-point function in an off-diagonal way. semi-fast

    compare Eq. (7.1) in [1]

    Args:
        one (np.ndarray): shape (N0, nsrc, ngamma)
        vol (float): volume of lattice
    """
    # T: time extent
    T, nsrc, ngamma = one.shape
    G = np.zeros(T, dtype=one.dtype)
    for t in range(T):
        for y0 in range(T):
            for ig in range(ngamma):
                mat = np.outer(
                    one[(t + y0) % T, :, ig],
                    one[y0, :, ig],
                )
                G[t] += np.sum(mat) - np.sum(np.diag(mat))
    G /= (-1) * nsrc * (nsrc - 1) * vol * ngamma
    assert np.linalg.norm(np.imag(G)) < eps
    return fold_correlator(np.real(G))


def convolute(one, vol, fold=True):
    """convolutes one-point function in an off-diagonal way. fast

    compare Eq. (7.1) in [1]

    Args:
        one (np.ndarray): shape (N0, nsrc, ngamma)
        vol (float): volume of lattice

    Returns:
        (np.ndarray): shape (N0/2)
    """
    if vol is None:
        raise TypeError("vol must not be None.")
    # T: time extent
    T, nsrc, ngamma = one.shape
    G = np.zeros(T, dtype=one.dtype)
    for t in range(T):
        one_roll = np.roll(one, t, axis=0)
        G[t] += np.einsum("tig,tjg->", one, one_roll)
        # subtract contribution from same noise source
        G[t] -= np.einsum("tig,tig->", one, one_roll)
    G /= (-1) * nsrc * (nsrc - 1) * vol * ngamma
    imag = np.linalg.norm(np.imag(G))
    assert imag < eps, f"imaginary part: {imag}"
    if fold:
        return fold_correlator(np.real(G))
    return np.real(G)


if __name__ == "__main__":
    import time

    np.random.seed(13)
    n0half = 32
    nsrc = 20
    ngamma = 3
    one = np.random.random((n0half, nsrc, ngamma))
    g1 = convolute(one, 1)

    for method in [
        convolute,
        convolute_medium,
        # convolute_std,
        convolute_naive,
        convolute_ensemble,
    ]:
        tic = time.perf_counter()
        if method.__name__ == convolute_ensemble.__name__:
            g2 = method(one[:, np.newaxis, ...], 1)[:, 0]
        else:
            g2 = method(one, 1)
        toc = time.perf_counter()
        diff = np.linalg.norm(g1 - g2)
        print(f"{method.__name__:20} {toc-tic:.2e} s", f"\tdiff = {diff:.2e}")
