"""generate plot for continuum limit. Example:
    python
"""
import re
import sys
import warnings

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
from scipy.stats import sem

from config.directories import GIT_COMMIT
from read.parse import arguments
from read.read_meson import read_data
from resample.resample import jackknife, jackknife_average


def average_onepoint(onepoint):
    """averages over gammas and sources, sums over flavors

    Args:
        onepoint (np.ndarray): shape (ncnfg, nsrc, nfl, ngamma, N0/2)

    Returns:
        np.ndarray: shape (ncnfg, N0/2)
    """
    if onepoint.ndim == 5:
        onepoint = np.sum(onepoint, axis=2) # sum over flavors
    else: # shape (ncnfg, nsrc, ngamma, N0)
        warnings.warn("Deprecated storage format of one-point")
    onepoint = np.real(np.mean(onepoint, axis=2)) # average over gammas

    if onepoint.shape[0] > 1: # several configs
        onepoint = np.mean(onepoint, axis=1) # average over sources
    else: # one config, treat sources as configs
        onepoint = onepoint[0, ...]
    return onepoint


def jackknife_onepoint(onepoint, x0=None):
    """performs jackknife on summed variance of one-point function

    Args:
        onepoint (np.ndarray): shape (ncnfg, N0)

    Returns:
        float, float: jackknife mean, jackknife standard deviation
    """
    onepoint_jk = jackknife(onepoint, axis=0) # shape (njk, ncnfg, N0)
    std_jk = sem(onepoint_jk, axis=1) # shape (njk, N0)
    if x0 is None: # average over time extent
        variance = np.mean(std_jk**2, axis=1)
    else:
        variance = std_jk[:,x0]**2
    mean, std = jackknife_average(variance, None, axis=0)
    return mean, std


def plot_qed(x, y, yerr, labels, show=False):
    """plots disconnected contribution without charge factor.

    Args:
        means (list): list of np.array with varying length
        stds (list): list of np.array with varying length
        labels (list): list of str
        show (bool): if True, show plot
    """
    filename = f"pdfs/variance_{args.ensemble.name}_{'_'.join(args.type)}.pdf"
    assert len(x) == len(y), f"len(x) = {len(x)} != {len(y)} = len(y)"
    N = len(x)

    with PdfPages(filename) as pdf:
        plt.plot(x[0], y[0][-1]*(x[0]/x[0][-1])**(-1.0), "k",
                 alpha=0.3, label=r"$(a/L)^{-1}$")
        plt.plot(x[0], y[1][-1]*(x[0]/x[0][-1])**(-3.0), "k",
                 alpha=0.6, label=r"$(a/L)^{-3}$")
        for i in range(N):
            plt.errorbar(x[i], y[i], yerr=yerr[i], fmt=".--", label=labels[i])
        plt.legend()
        plt.xlabel(r"$a/L$")
        plt.ylabel(r"$(a/L) \sum_{x_0} \sigma^2(x_0)$")
        plt.xscale("log")
        plt.yscale("log")
        plt.title(f"Variance {args.ensemble.name}")
        metadata = pdf.infodict()
        metadata["Subject"] = f"git commit: {GIT_COMMIT[:8]}, command: {' '.join(sys.argv)}"
        plt.grid(which="major", axis="y")
        pdf.savefig()
        if show:
            plt.show()
        plt.close()

def continuum_limit(args):
    """create continuum limit plot with
    python -m disconnected.plot_qed --ensemble ib8 --type same_charge \
    --type same_mass --type same_charge_split_even
    """
    lattices = np.array([8, 16, 24, 32])
    values = []
    errors = []
    scale = 1e4
    summed = [np.zeros((len(args.ensemble.configs), N)) for N in lattices]
    for k, curr_type in enumerate(args.type):
        stds_jk = np.zeros(len(lattices))
        err = np.zeros(len(lattices))
        for i, N in enumerate(lattices):
            args.ensemble.runname = f"ib{int(N)}"
            onepoint = read_data(args.ensemble, curr_type)
            if curr_type in ["same_mass", "same_charge"]:
                summed[i] += onepoint
            stds_jk[i], err[i] = jackknife_onepoint(onepoint, x0=None)
        if "same_charge" in curr_type:
            stds_jk *= scale
            err *= scale
            args.type[k] = f"{scale:.0g}_{curr_type}"
        values += [stds_jk]
        errors += [err]

    stds_jk = np.zeros(len(lattices))
    err = np.zeros(len(lattices))
    for i, N in enumerate(lattices):
        stds_jk[i], err[i] = jackknife_onepoint(summed[i], x0=None)
    values += [stds_jk]
    errors += [err]
    args.type += ["same_charge_plus_same_mass"]

    args.ensemble.name = "qed"
    plot_qed(len(values) * [1/lattices], values, errors, args.type, show=True)

def charge_limit(args):
    """investigate scaling for e -> 0
    python -m disconnected.plot_qed --ensemble ib24 --type qed2 --type qed4 --type qed6 --type qed10
    """
    x = np.zeros(len(args.type))
    y = np.zeros(len(args.type))
    yerr = np.zeros(len(args.type))
    for i, curr_type in enumerate(args.type):
        onepoint = read_data(args.ensemble, curr_type)
        onepoint = average_onepoint(onepoint)
        std = sem(onepoint, axis=0) # shape (njk, N0)
        print(np.mean(std**2))
        y[i], yerr[i] = jackknife_onepoint(onepoint)
        m = re.findall(r"\d+", curr_type)
        assert m, f"No integer charge found in curr_type {curr_type}"
        assert len(m) == 1, f"Multiple integers found in curr_type {curr_type}"
        x[i] = float(m[0])

    print(y)
    plt.errorbar(x, y, yerr=yerr, fmt=".--", label=args.ensemble.name)
    # x = np.insert(x, 0, 0.01)
    if len(y) > 1:
        alpha = (y[-1] - y[1]) / (x[-1] - x[1])
        beta = y[-1] - alpha*x[-1]
        plt.plot(np.insert(x, 0, 0.01), alpha*np.insert(x, 0, 0.01)+beta,
                "k", alpha=0.6, label=f"{alpha:.3f} "+r"$qhat$"+f"+{beta:.2f}")
    plt.legend()
    plt.xlabel(r"qhat")
    plt.ylabel(r"$(a/L) \sum_{x_0} \sigma^2(x_0)$")
    # plt.xscale("log")
    # plt.yscale("log")
    plt.title("Variance ib")
    filename = f"pdfs/variance_charge_{args.ensemble.name}.pdf"
    with PdfPages(filename) as pdf:
        metadata = pdf.infodict()
        metadata["Subject"] = f"git commit: {GIT_COMMIT[:8]}, command: {' '.join(sys.argv)}"
        pdf.savefig()
        plt.show()
        plt.close()

if __name__ == "__main__":
    args = arguments()

    # continuum_limit(args)
    charge_limit(args)
