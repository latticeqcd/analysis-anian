"""Calculates disconnected contribution of free Dirac operator.

References: 
  [1]     Lattice Gauge Theories, Heinz J. Rothe
  [2]     Overleaf notes
"""
import itertools

import numpy as np

from disconnected.read_disconnected import read_disconnected_diff_free, read_disconnected_free
from read.read_meson import fold_correlator

def m(m0, p):
    """mass with Wilson doublers

    Args:
        m0 (float): bare mass
        p (np.array): length 4

    Returns:
        float: mass with Wilson doublers
    """
    return m0 + 2*np.sum(np.sin(p/2)**2)

def integrand(m0, p):
    """integrand in Eq. (3.49) of [2]

    Args:
        m0 (float): bare mass
        p (np.array): momentum, length 4

    Returns:
        float: contribution for p
    """

    mp = m(m0, p)
    denominator = mp**2 + np.sum(np.sin(p)**2)
    if abs(denominator) < 1e-10:
        return -12 / 4
    return -12 * mp / denominator
      

def disconnected(m0, q, N):
    """disconnected contribution for a single flavor with bare mass m0
    and charge q.

    Args:
        m0 (float): bare mass
        q (float): charge
        N (np.array): lattice dimensions, length 4

    Returns:
        float: G(t)
    """
    contr = 0.0
    c = np.zeros_like(N)
    c[0] = 1.0
    for i in itertools.product(*map(range, N)):
        p = np.pi/N * (2.0*np.array(i)+c)
        contr += integrand(m0, p)
    return q/N[0]*contr

def ensemble(kappas, qs, N):
    # kappas = np.array([0.13440733, 0.12784])
    # qs = [1/3, -1/3]
    m0 = 1/(2*kappas) - 4
    contracted = np.zeros((N[0], len(kappas)))
    for i in range(len(kappas)):
        contracted[:,i] = disconnected(m0[i], qs[i], N)
    return np.mean(contracted, axis=0)


def plot_exact(kappa, q, N):
    m0 = 1/(2*kappa) - 4
    contracted = np.zeros(len(kappa))
    for i in range(len(kappa)):
        contracted[i] = disconnected(m0[i], q, N)
    return contracted


def mean(corr, axis=None):
    mu = np.mean(corr, axis=axis)
    if axis:
        samples = np.prod([corr.shape[a] for a in axis])
    else:
        samples = np.prod(corr.shape)
    std = np.std(corr, axis=axis) / np.sqrt(samples)
    if isinstance(mu, float):
        return ufloat(mu, std)
    else:
        return np.array([ufloat(mu[i], std[i]) for i in range(len(mu))])


if __name__ == "__main__":
    from config.directories import ensembles
    from uncertainties import ufloat
    kappas = np.array([0.12360817, 0.11803188])
    qs = np.array([1/3, -1/3])
    volume = np.array([16,8,8,8])
    exact = ensemble(kappas, qs, volume)
    print("------- inverse Dirac trace -------------")
    print(f"exact: {np.sum(exact):+.2f},\t\t {exact[0]:+.2f}\t {exact[1]:+.2f}")
    qcd0 = ensembles["qcd0"]
    qcd0.flavors = ["0", "1"]
    qcd0.sources = np.arange(10)

    corr1 = read_disconnected_free(qcd0, "disc_loc_loc")
    corrsum = mean(np.sum(corr1, axis=1))
    corr1mean = mean(corr1, (0,2))
    print(f"naive: {corrsum},\t {corr1mean[0]:+.2f}, {corr1mean[1]:+.2f}")

    corr2 = read_disconnected_diff_free(qcd0, "disc_loc_loc")
    print(f"diff: {mean(corr2):.5f}")

    import matplotlib.pyplot as plt
    corr1 = fold_correlator(np.transpose(np.transpose(np.sum(corr1,axis=1))))
    plt.errorbar(np.arange(corr1.shape[0]), np.mean(corr1,axis=1), yerr=np.std(corr1,axis=1)/np.sqrt(corr1.shape[1]), fmt='.', label="naive")
    corr2 = fold_correlator(np.transpose(corr2))
    plt.errorbar(np.arange(corr2.shape[0]), np.mean(corr2,axis=1), yerr=np.std(corr2,axis=1)/np.sqrt(corr2.shape[1]), fmt='.', label="diff")
    plt.legend()
    plt.show()