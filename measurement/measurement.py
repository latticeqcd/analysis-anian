"""custom class to handle floats with errors.

makes use of the uncertainties library
(https://pythonhosted.org/uncertainties/index.html), but adds the
possibility to have arrays of float with errors."""
from collections.abc import MutableSequence
import copy

import matplotlib.pyplot as plt
import numpy as np
from simplejson import JSONDecoder, JSONEncoder

import uncertainties

def m2dict(measurement):
    """converts a M object to a dictionary.

    Args:
        measurement (M): has value and error.
    """
    return {"value": measurement.value, "error": measurement.error}


def marray2dict(array):
    """converts an array of measurements to a dict.

    Args:
        array (np.ndarray): several measurements of same observable.

    Returns:
        dict: dictionary
    """
    return {"value": np.mean(array), "error": np.std(array)}


class M(uncertainties.core.Variable):
    """class for measurements. Each instance has a value and an error.

    Examples:

        v = M(0.3, 0.01)  # has value 0.3 and error 0.01
        w = M(0.3)  # has value 0.3 and error 0.0
    """

    def __init__(self, value, error=None):
        if error is None:
            error = 0.0
        super().__init__(value, error)

    @property
    def value(self):
        return self.n

    @value.setter
    def value(self, v):
        self.n = v

    @property
    def error(self):
        return self.s

    @error.setter
    def error(self, e):
        self.s = e

    @classmethod
    def from_array(cls, measurements, **kwargs):
        """converts an array to a measurement by taking its mean
        and std. keyword arguments can be used to specify the axis."""
        mean = np.mean(measurements, **kwargs)
        std = np.std(measurements, **kwargs)
        return cls(mean, std)

    def sample(self):
        """samples from measurement assuming a normal distribution with
        mean self.value and standard deviation self.error."""
        return np.random.normal(self.value, self.error)


class MEncoder(JSONEncoder):
    def default(self, object):
        if isinstance(object, M):
            return {"value": object.value, "error": object.error}
        return super().default(object)


class MArray(MutableSequence):
    """class for array of measurements. Each instance consists of a list
    of type M. See also main function below.

    Examples:
        x = MArray([0.3, 0.2, 0.1], [0.01, 0.01, 0.02])
    """
    def __init__(self, value=None, error=None):
        if isinstance(value, (list, np.ndarray)):
            self.list = value
        if value is None:
            value = np.array([])
        if error is None:
            error = np.zeros(value.shape)
        self.list = [M(v, e) for v, e in zip(value, error)]

    @classmethod
    def from_array(cls, list):
        el = cls()
        el.list = list
        return el

    def __len__(self):
        return len(self.list)

    def __getitem__(self, i):
        return self.list[i]

    def __delitem__(self, i):
        del self.list[i]

    def __setitem__(self, i, val):
        self.list[i] = val

    def insert(self, index, value):
        self.list.insert(index, value)

    def __str__(self):
        return "[" + ", ".join([str(el) for el in self.list]) + "]"

    def plot(self, ax=None, **kwargs):
        """plots array of measurement with error bars."""
        arguments = {"x": np.arange(len(self)), "y": self.value,
                     "yerr": self.error, **kwargs}
        if ax is not None:
            ax.errorbar(**arguments)
            return ax
        plt.errorbar(**arguments)
        return None

    @property
    def value(self):
        return np.array([element.value for element in self.list])

    @property
    def error(self):
        return np.array([element.error for element in self.list])

    def __deepcopy__(self, copy_array):
        copy.deepcopy(self.list, copy_array)
        return MArray.from_array([M(val.n, val.s) for val in self.list])

    def average(self):
        """returns the mean and the summed squares of the errors."""
        std = np.linalg.norm(self.error) / len(self)
        mean = M(np.mean(self.value), std)
        return mean


if __name__ == "__main__":
    length = M(0.3, 0.1)
    assert str(M(0.3, 0.1)) == "0.30+/-0.10"
    assert str(M(0.3)) == "0.3+/-0"
    assert str(length.value) == "0.3"
    assert str(length.n) == "0.3"
    assert str(length.error) == "0.1"

    lengths = MArray([0.3, 0.31, 0.32], [0.01, 0.02, 0.03])
    assert str(lengths) == "[0.300+/-0.010, 0.310+/-0.020, 0.320+/-0.030]"
    assert str(lengths.average()) == "0.310+/-0.012"
    assert len(lengths) == 3
    assert (lengths.value == [0.3, 0.31, 0.32]).all()
    assert (lengths.error == [0.01, 0.02, 0.03]).all()
    assert str(lengths[0]) == "0.300+/-0.010"
    lengths[0] = M(0.3, 0.1)
    assert str(lengths[0]) == "0.30+/-0.10"

    lengths2 = MArray()
    lengths2.append(M(0.3, 0.2))
    assert str(lengths2) == "[0.30+/-0.20]"

    np.random.seed(13)
    measurements = np.random.random(1000)
    result = M.from_array(measurements, axis=0)
    assert str(result) == "0.49+/-0.29"

    matrix_array = MArray.from_array([lengths, lengths])
    print(matrix_array.error)
