"""resample techniques such as bootstrap and jackknife"""
import numpy as np


def bootstrap(my_array, axis=0):
    """select `nsamples` samples from correlators to generate a
    bootstrap, where `nsamples` is the dimension of the axis of
    that we want to bootstrap.

    Args:
        correlators (np.ndarray): arbitrary shape

    Returns:
        (np.ndarray): array with same shape as `my_array`.
    """
    if isinstance(my_array, list):
        my_array = np.array(my_array)

    transposition = np.arange(my_array.ndim)
    transposition[axis] = 0
    transposition[0] = axis
    transposed = np.transpose(my_array, transposition)

    nsamples = np.shape(transposed)[0]
    bootstrapped = np.zeros_like(transposed)
    for i in range(nsamples):
        random_i = np.random.randint(0, nsamples)
        bootstrapped[i, ...] = transposed[random_i, ...]

    return np.transpose(bootstrapped, transposition)


def jackknife(my_array, axis=0, max_samples=None):
    """performs jackknife on `my_array`.

    Args:
        my_array (np.ndarray): has nsamples = np.shape(my_array)[axis]
        axis (int, optional): axis over which to generate jackknife samples.
        max_samples (int): maximal number of samples, default is
            my_array.shape[0]

    Returns:
        np.ndarray: has shape (nsamples, np.shape(my_array))
    """
    if isinstance(my_array, list):
        my_array = np.array(my_array)

    transposition = np.arange(my_array.ndim)
    transposition[axis] = 0
    transposition[0] = axis
    transposed = np.transpose(my_array, transposition)

    shape = np.shape(transposed)
    if max_samples is None:
        max_samples = shape[0]
    max_samples = min(shape[0], max_samples)
    dtype = float
    if my_array.dtype == complex:
        dtype = complex

    jackknived = np.zeros((max_samples, shape[0] - 1, *shape[1:]), dtype=dtype)
    for i in range(min(max_samples, shape[0])):
        jackknived[i, ...] = np.concatenate(
            (transposed[:i, ...], transposed[i + 1 :, ...]), axis=0
        )

    transposition = np.arange(jackknived.ndim)
    transposition[axis + 1] = 1
    transposition[1] = axis + 1
    return np.transpose(jackknived, transposition)


def jackknife_average(array, mean0=None, axis=0):
    """yields average and error of jackknife sampling.

    According to Gattringer & Lang Eq. (4.71)

    Args:
        array (np.ndarray): shape (nsamples, ...)
        mean0 (np.ndarray): mean of sample (...)

    Returns:
        (..., ...): jackknife mean, std
    """
    N = array.shape[axis]
    mean = np.mean(array, axis=axis)
    if mean0 is None:
        mean0 = mean
    if mean0.shape != array.shape:
        assert mean0.ndim == array.ndim - 1, \
            f"mean0.ndim = {mean0.ndim} != {array.ndim} = array.ndim"
        # avoid error due to missing newaxis
        transpose = np.arange(array.ndim)
        transpose[0] = axis
        transpose[axis] = 0
        # broadcast mean
        meanbc = np.transpose(mean0[np.newaxis, ...], transpose)
    else:
        meanbc = mean0
    var = (N - 1) / N * np.sum((array - meanbc) ** 2, axis=axis)

    return mean0 - (N - 1) * (mean - mean0), np.sqrt(var)


if __name__ == "__main__":
    ncnfg = 10
    N0 = 2
    axis = 0  # axis where configurations are

    x = np.random.random((ncnfg, N0))

    # shape (njk, ncnfg-1, N0) where njk = ncnfg
    x_jackknife = jackknife(x)  # shape (njk, ncnfg-1, N0)
    norm = np.linalg.norm(x_jackknife[0, ...] - x[1:, ...])
    assert np.allclose(x[1:, ...], x_jackknife[0, ...])

    # standard way
    mean0, std0 = np.mean(x, axis=axis), np.std(x, axis=axis) / np.sqrt(
        x.shape[axis] - 1
    )
    mean1, std1 = jackknife_average(np.mean(x_jackknife, axis=axis + 1), mean0, axis=0)
    print(f"Standard way: {mean0} +/- {std0}")
    print(f"Jackknife:    {mean1} +/- {std1}")
    assert np.linalg.norm(mean0 - mean1) < 1e-14
    assert np.linalg.norm(std0 - std1) < 1e-14

    x_bootstrap = bootstrap(x)
    for i in range(ncnfg):
        assert x_bootstrap[i, ...] in x
