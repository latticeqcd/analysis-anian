"""finds pion mass from main/ms6.c, example
    python -m fitting.pion --ensemble ib8 --type pion
"""
from argparse import ArgumentParser
import os
import sys

# import uncertainties
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
from config.directories import GIT_COMMIT

# from plot.correlator import plot_correlator
from fitting.mass import one_parameter_fit, cosh_plot
from read.read_meson import fold_correlator
from read.read_ms6 import read_ms6
from read.parse import base_args, process_base_args

if __name__ == "__main__":
    parser = ArgumentParser()
    parser = base_args(parser)
    args = parser.parse_args()
    args = process_base_args(args)

    Ls = np.array([8, 16, 24, 32])
    fit_ranges = {"8": np.arange(0, 3), "16": np.arange(3, 6),
                  "24": np.arange(4, 8), "32": np.arange(8, 13)}

    with PdfPages("pdfs/pion_qed.pdf") as pdf:
        fig, ax = plt.subplots()
        for L in Ls:
            args.ensemble.runname = f"ib{L}"
            args.fit_range = fit_ranges[str(L)]
            infile = os.path.join(args.ensemble.dir, args.type[0],
                f"{args.ensemble.runname}.cnfg{args.ensemble.configs[0]}.ms6.dat")
            p = read_ms6(infile, args.ensemble.configs)
            corr = fold_correlator(p)
            corr = corr[...,0]
            mass, out = one_parameter_fit(corr, args.fit_range, plot=False)
            print(f"mass: {mass[0]} +/- {mass[1]}")
            cosh_plot(corr)
            # plot_correlator([L*corr[...,0]], [f"L/a = {L}"], ax=ax)
        plt.legend()
        plt.xlabel(r"$x_0/L$")
        plt.ylabel(r"$m_{eff} \, L$")
        plt.title("Effective mass for pseudoscalar correlator")
        metadata = pdf.infodict()
        metadata["Subject"] = f"git commit: {GIT_COMMIT[:8]}, command: {' '.join(sys.argv)}"
        pdf.savefig()
        plt.show()
