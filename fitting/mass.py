"""extracts mass from a cosh-fit"""
import warnings

from logging import warning
import lmfit
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import fsolve
from scipy.stats import sem

from fitting.functions import cosh_ratio, log_cosh_fit, cosh_fit
from read.read_meson import get_correlator_ensemble
from resample.resample import jackknife

# p-value 0.05 for different numbers of degrees of freedom,
# source: https://en.wikipedia.org/wiki/Chi-squared_distribution#Table_of_%CF%872_values_vs_p-values
# https://people.richland.edu/james/lecture/m170/tbl-chi.html
chisquare = [3.84, 5.99, 7.81, 9.49, 11.07, 12.59, 14.07,
             15.51, 16.92, 18.31, 19.675, 21.026, 22.362, 23.685,
             24.996, 26.296, 27.587, 28.869, 30.144, 31.410, 32.671]


def cosh_plot(correlators):
    """solve ratio of
        C(t) = A cosh(m*(t-N0/2)).
    for each time t.

    Args:
        correlators (np.ndarray): folded correlators, shape (N0/2,ncnfg)
    """

    N0half, ncnfg = correlators.shape
    N0 = 2 * N0half
    corr_jk = jackknife(correlators, axis=1) # shape (njk=ncnfg, N0/2, ncfng-1)
    ratio = corr_jk[:,:-1,:] / corr_jk[:,1:,:]
    mean = np.mean(ratio, axis=2)
    masses = np.zeros_like(mean)
    for i in range(mean.shape[0]):
        for t in range(mean.shape[1]):
            masses[i, t] = fsolve(lambda mass: cosh_ratio(N0, t, mass) - mean[i, t], 0.5)
    plt.errorbar(np.arange(N0half-1) / N0, np.mean(masses, axis=0) * N0,
                 yerr=sem(masses, axis=0)*N0, fmt=".-", label=f"L/a={N0}")


def get_inv_cov(array, diagonal):
    """prepare inverse sqrt of covariance matrix

    Args:
        array (np.ndarray): shape (N0/2, ncnfg)
        diagonal (bool): if True, only use diagonal covariance matrix.

    Returns:
        np.ndarray: inverse covariance matrix, shape (N0/2, N0/2)
    """
    if diagonal:
        with warnings.catch_warnings():
            # disable run time warning due to dividing by zero
            warnings.simplefilter("ignore")
            diag = np.diag(1/np.nanstd(array, axis=1))
        return diag
        # return diag / np.linalg.norm(diag)

    cov = np.cov(array)
    D, U = np.linalg.eigh(cov)
    valid = np.where(D > 0)[0]
    Dinv = np.zeros(len(D))
    Dinv[valid] = 1.0/D[valid]
    inv = U @ np.diag(np.sqrt(Dinv)) @ U.T
    return inv


def one_parameter_fit(correlators, fit_range, diagonal=True, plot=False):
    """fits mass m in correlator to
        C(t) = A cosh(m*(t-N0/2)).

    Args:
        correlators (np.ndarray): folded correlators, shape (N0/2,ncnfg)
        fit_range (np.ndarray): range to take into account for fitting.
        diagonal (bool): Use only diagonal covariance elements.

    Returns:
        mass (float): mass
        stderr (float): error on mass
    """
    if correlators.dtype == complex:
        warning("discarding imaginary part [one_parameter_fit]", )
        correlators = np.real(correlators)
    N0 = 2*np.shape(correlators)[0]
    with warnings.catch_warnings():
        # disable run time warning due to negative logs
        warnings.simplefilter("ignore")
        log_ratio = np.log(correlators[:-1,:]/correlators[1:,:])
    if np.all(np.isnan(log_ratio[0,:])):
        # warnings.warn("log_ratio[0,:] is nan -> set to 1")
        log_ratio[0,:] = np.ones(correlators.shape[1])
    mean = np.nanmean(log_ratio, axis=1)
    inv = get_inv_cov(log_ratio, diagonal)

    def residual(params, x, data, inv):
        t = np.arange(int(N0/2)-1)
        mass = params['mass']
        return (inv @ (data-log_cosh_fit(N0, t, mass)))[x]

    params = lmfit.Parameters()
    params.add('mass', value=0.5, min=0.0)
    out = lmfit.minimize(residual, params, args=(fit_range, mean, inv), nan_policy="omit")
    mass = out.params["mass"].value

    if plot:
        plt.vlines([fit_range[0], fit_range[-1]], ymin=min(mean),
                   ymax=max(mean), colors="k", linestyles="--",
                   label="fit range")
        plt.errorbar(np.arange(len(mean)), mean, yerr=np.sqrt(1/np.diag(inv)),
                     fmt='.', label="log ratio")
        plt.plot(mass * np.ones(len(mean)), label="fit mass")
        plt.legend()
        plt.show()
    if out.chisqr > chisquare[len(fit_range)-1-1]:
        pass
        # warning(f"Bad reduced chi square: {out.redchi}")
    return (mass, out.params["mass"].stderr), out


def amplitude_fit(correlators, fit_range, mass, diagonal=True):
    """fits mass m and amplitude A in correlator to
        C(t) = A cosh(m*(t-N0/2)).

    Args:
        correlators (np.ndarray): folded correlators, shape (N0/2,ncnfg)
        fit_range (np.ndarray): range to take into account for fitting.
        diagonal (bool): Use only diagonal covariance elements.

    Returns:
        (float, float): mass and amplitude
        (MinimizerResult): Object containing the optimized parameters
            and several goodness-of-fit statistics.
    """
    if correlators.dtype == complex:
        warning("discarding imaginary part [two_parameter_fit]")
        correlators = np.real(correlators)
    N0 = 2*np.shape(correlators)[0]
    t = np.arange(int(N0/2))
    mean = np.nanmean(correlators, axis=1)
    inv = get_inv_cov(correlators, diagonal)

    def residual(params, x, data, inv, mass):
        ampl = params['ampl']
        return (inv @ (data-cosh_fit(N0, t, mass, ampl)))[x]

    params = lmfit.Parameters()
    params.add('mass', value=0.5, min=0.0)
    params.add('ampl', value=0.002)
    out = lmfit.minimize(residual, params, args=(fit_range, mean, inv, mass))
    ampl = out.params["ampl"].value
    if out.chisqr > chisquare[len(fit_range)-2-1]:
        warning(f"Bad reduced chi square: {out.redchi}")
    return ampl, out



def two_parameter_fit(correlators, fit_range, diagonal=True):
    """fits mass m and amplitude A in correlator to
        C(t) = A cosh(m*(t-N0/2)).

    Args:
        correlators (np.ndarray): folded correlators, shape (N0/2,ncnfg)
        fit_range (np.ndarray): range to take into account for fitting.
        diagonal (bool): Use only diagonal covariance elements.

    Returns:
        (float, float): mass and amplitude
        (MinimizerResult): Object containing the optimized parameters
            and several goodness-of-fit statistics.
    """
    if correlators.dtype == complex:
        warning("discarding imaginary part [two_parameter_fit]")
        correlators = np.real(correlators)
    N0 = 2*np.shape(correlators)[0]
    t = np.arange(int(N0/2))
    mean = np.nanmean(correlators, axis=1)
    inv = get_inv_cov(correlators, diagonal)

    def residual(params, x, data, inv):
        mass = params['mass']
        ampl = params['ampl']
        return (inv @ (data-cosh_fit(N0, t, mass, ampl)))[x]

    params = lmfit.Parameters()
    params.add('mass', value=0.5, min=0.0)
    params.add('ampl', value=0.002)
    out = lmfit.minimize(residual, params, args=(fit_range, mean, inv))
    mass = out.params["mass"].value
    ampl = out.params["ampl"].value
    res = np.linalg.norm((inv @ (mean-cosh_fit(N0,t,mass,ampl)))[fit_range])
    if out.chisqr > chisquare[len(fit_range)-2-1]:
        warning(f"Bad reduced chi square: {out.redchi}")
    return (mass, ampl), out


if __name__ == "__main__":
    from read.parse import arguments

    args = arguments()

    correlator = get_correlator_ensemble(args.ensemble, args.type)
    N0 = 2*correlator.shape[0]
    (mass, _), _ = one_parameter_fit(correlator, args.ensemble.fit_range)
    ampl, _ = amplitude_fit(correlator, args.ensemble.fit_range, mass)
    print(mass, ampl)

    plt.errorbar(np.arange(int(N0/2)), np.mean(correlator, axis=1),
                 np.std(correlator, axis=1)/np.sqrt(correlator.shape[1]),
                 fmt=".", label="data")
    t = np.linspace(0, int(N0/2), 1000)
    plt.plot(t, cosh_fit(N0, t, mass, ampl), label="fit")
    plt.yscale("log")
    plt.show()
