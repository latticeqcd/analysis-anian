"""contains fit functions"""
import numpy as np

# pylint: disable=unnecessary-lambda-assignment

# compare with Gattringer & Lang, Eq. (6.59)
cosh_fit = lambda N0, t, mass, ampl: 2 * ampl * np.exp(-mass*N0/2) * np.cosh((t-N0/2)*mass)
# compare with Gattringer & Lang, Eq. (6.55), not currently used
cosh_fit_2 = lambda N0, t, mass, ampl: ampl * np.cosh((t-N0/2)*mass)
# compare with Gattringer & Lang, Eq. (6.57)
cosh_ratio = lambda N0, t, mass: np.cosh((t-N0/2)*mass) / np.cosh((t+1-N0/2)*mass)
log_cosh_fit = lambda N0, t, mass: np.log( np.cosh((t-N0/2)*mass) / np.cosh((t+1-N0/2)*mass))
# corresponds to cosh_fit for small times
exp_fit = lambda t, mass, ampl: ampl * np.exp(-mass*t)
