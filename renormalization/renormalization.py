"""Methods for calculating the vector renormalization constant.
"""
import os

import numpy as np
import matplotlib.pyplot as plt

from uncertainties import ufloat

from read.parse import renormalization_arguments
from read.read_meson import read_meson_file
from resample.resample import jackknife, jackknife_average


def read_PP_PVP(base, runname, configs):
    """Reads P(x0) P(z0) and P(x0) V(y0) P(z0) for each configuration.

    Args:
        base (str): base directory
        runname (str): runname of ensemble
        configs (np.array): array of configurations

    Returns:
        (np.ndarray, np.ndarray): PP and PVP, shape (len(configs), N0)
    """
    PP = []
    PVP = []
    for cnfg in configs:
        filename = os.path.join(base, runname + "_n" + str(cnfg))
        PP += [read_meson_file(filename + "_PP.dat")]
        PVP += [read_meson_file(filename + "_PVP.dat")]
    return np.array(PP), np.array(PVP)


def get_ratio(PP, PVP, fit):
    """Calculates ratio PVP / PP

    Args:
        PP (np.ndarray): shape (ncnfg, N0)
        PVP (np.ndarray): shape (ncnfg, N0)

    Returns:
        np.array: ratio, length N0/2
    """
    N0 = PP.shape[1]
    N02 = int(N0/2)
    x0 = int(3*N0/4)
    z0 = int(N0/4)
    z0m1 = (N0+z0-1)%N0
    PVP -= PVP[:,z0m1:z0m1+1]
    PP0 = PP[:,x0:x0+1]
    if PVP.shape[0] == 1:
        ratio = np.real(PVP[0,...] / PP0[0,...])
        ratio = np.roll(ratio, -z0)
        inv_ratio = 1 / (ratio[:N02] - ratio[N02:])
        ZV = np.mean(inv_ratio[fit])
        err = np.std(inv_ratio[fit])
        return (inv_ratio, np.zeros(N02)), ufloat(ZV, err)

    # PVP.shape[0] > 1
    PP0 = jackknife(PP0)
    PVP = jackknife(PVP)
    inv_ratio = np.zeros((PVP.shape[0], N02))
    ZV = np.zeros(PVP.shape[0])
    for i in range(PVP.shape[0]):
        ratio = np.real(np.mean(PVP[i,...], axis=0) / np.mean(PP0[i,...], axis=0))
        ratio = np.roll(ratio, -z0)
        inv_ratio[i,:] = 1 / (ratio[:N02] - ratio[N02:])
        ZV[i] = np.mean(inv_ratio[i, fit])
    ZV = ufloat(*jackknife_average(ZV))

    return jackknife_average(inv_ratio), ZV

def plot_ratio(x, y, err, ZV, fit, label=""):
    """Plots the ratio.

    Args:
        x (np.array): x values
        y (np.array): ratio values
        err (np.array): std of ratio
        ZV (uncertainties.ufloat): value and err of ZV
        label (str): label for plot
    """
    p = plt.errorbar(x, y, err, fmt=".", label=label)
    plt.hlines(ZV.n, fit[0], fit[-1], color=p[0].get_color(), label=f"ZV = {ZV:.4f}")
    plt.fill_between(fit, ZV.n-ZV.s, ZV.n+ZV.s, color=p[0].get_color(), alpha=0.2)
    plt.xlabel(r"$(y_0 - z_0) / a$")
    plt.ylabel(r"$Z_V$")


if __name__ == "__main__":
    args = renormalization_arguments()
    ensemble = args.ensemble

    i = 0
    PP, PVP = read_PP_PVP(os.path.join(ensemble.dir, args.type[i]),
                          ensemble.runname, ensemble.configs)
    (ratio, std), ZV = get_ratio(PP, PVP, args.fit_range)
    print(ZV)
    plot_ratio(np.arange(1, len(ratio)), ratio[1:], std[1:], ZV,
               args.fit_range, f"{args.type[i]}")

    # ZV_ref = ufloat(0.74803, 0.00021)
    # diff = ZV - ZV_ref
    # print("difference:", diff, f", {(diff.n / diff.s):.1f} sigmas")
    # plt.hlines(ZV_ref.n, args.fit_range[0], args.fit_range[-1],
    #            color="k", label=f"ZV_ref = {ZV_ref}")
    # plt.fill_between(args.fit_range, ZV_ref.n-ZV_ref.s, ZV_ref.n+ZV_ref.s, color="k", alpha=0.2)
    plt.legend()
    plt.title(f"ZV {ensemble.name} ({len(args.configs)} configs)")
    plt.savefig(f"plot/renormalization_{ensemble.name}.pdf")
    plt.show()
