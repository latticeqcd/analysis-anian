"""plots correlator for an ensemble."""
import argparse
import os

import matplotlib.pyplot as plt
import numpy as np

from config.directories import Ensemble
from read.parse import arguments_plot
from read.read_meson import fold_correlator, get_correlator, read_meson_file, get_connected

save_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "figures")
log = True

def plot_correlator(correlators, labels, show=False, ax=None):
    """plots different correlators

    Args:
        correlator (list): different correlators, each element of the
            list is a np.ndarray of shape (N0/2, ncnfg)
        label (list): each element is a str which is used as a label for
            the plot.

    """
    if ax is None:
        fig, ax = plt.subplots()
    if isinstance(correlators, np.ndarray):
        correlators = [correlators]
    if isinstance(labels, str):
        labels = [labels]
    assert len(correlators) == len(labels), f"{len(correlators)}, {labels}"
    for i in range(len(correlators)):
        data = np.real(correlators[i])
        N0, ncnfg = np.shape(data)
        ax.errorbar(np.arange(N0)/N0, np.mean(data, axis=1),
                    yerr=np.std(data, axis=1)/np.sqrt(ncnfg),
                    fmt=".-", label=labels[i])
        # ax.set_yscale("log")
    if len(correlators) == 2:
        mean0 = np.mean(correlators[0], axis=1)
        mean1 = np.mean(correlators[1], axis=1)
        diff = np.linalg.norm(mean0-mean1)
        reldiff = np.linalg.norm((mean0-mean1)/mean0)
        print(f"diff: {diff:.3g}, relative: {reldiff:.3g}")
    if show:
        plt.legend()
        plt.savefig("pdfs/currents.pdf")
        plt.show()
    else:
        return ax




if __name__ == "__main__":
    """run with python -m plot.correlator from git root directory
        python -m plot.correlator --ensemble A5c --type connected0 --suffix _f00_t1 --configs 5 205 5
    """
    from config.directories import ensembles
    legacy = False

    args = arguments_plot()
    correlators = []
    if args.suffix is not None:
        # delete first underscore
        labels = [d + " " + s[1:] for d in args.type for s in args.suffix]
    elif args.type is not None:
        labels = args.type
    else:
        labels = args.dir

    if legacy:
        correlators = [get_correlator(d, args.runname, args.configs, args.sources) for d in args.dir]
    else:
        correlators = [get_connected(d, args.runname, args.configs, suffix) for d in args.dir for suffix in args.suffix]

    # c2 = []
    # for file in os.listdir(args.dir[1]):
    #     if file.endswith(".dat"):
    #         file = os.path.join(args.dir[1], file)
    #         c2 += [read_meson_file(file)]
    # correlators += [np.transpose(c2)]
    for corr in correlators:
        assert np.linalg.norm(np.imag(corr)) < 1e-10
    folded = [fold_correlator(c.T)[1:] for c in correlators]

    # adjoint = [folded[0]+folded[1], folded[0]+folded[1]+4*folded[2],
    #            folded[0]+folded[1]+folded[2]+9*folded[3],
    #            folded[0]+folded[1]+folded[2]+folded[3]]
    for fold in folded:
        mean, err = np.mean(fold, axis=1), np.std(fold, axis=1) / np.sqrt(fold.shape[1])
        print(mean[0], err[0])
    plot_correlator(folded, labels, show=True)
