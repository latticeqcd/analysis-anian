"""functions related to evaluated gm2. run with
    python -m connected.gm2 --ensemble C380a50b324 --type ju_0_loc_loc
"""
# pylint: disable=unnecessary-lambda-assignment,import-error,no-name-in-module
import os
import time

import matplotlib.pyplot as plt
import numpy as np
import uncertainties

from fitting.mass import amplitude_fit, one_parameter_fit
from kernel.kernel import LOKernel, NLOKernel
from read.parse import connected_arguments
from read.read_meson import fold_correlator, get_correlator
from resample.resample import jackknife, jackknife_average


def gm2(correlator, kernel, ensemble, curr_type, cutoff, fit_range, plot=False,
        color="", interpolate=True, window_parms=None):
    """calculate gm2 for correlator

    Args:
        correlator (np.ndarray): shape (N0/2, ncnfg)
        ensemble (config.directories.Ensemble): ensemble
        plot (bool): if True, plot
        color (str): color for plot
        interpolate (bool): Use cubic-spline interpolation
        window_parms (tuple): Restrict to window with parameters
            (t0, t1, Delta)

    Returns:
        float: value of gm2
    """
    correlator = fold_correlator(correlator)
    (mass, _), _ = one_parameter_fit(correlator, fit_range)
    ampl, _ = amplitude_fit(correlator, fit_range, mass)
    total_int = kernel.parts_integrate(
        np.mean(correlator, axis=1), cutoff, mass, ensemble.m_mu(),
        interpolate=interpolate, window_parms=window_parms)

    if plot:
        N0half = correlator.shape[0]
        kernel0 = kernel.get_kernel(np.arange(N0half), ensemble.m_mu(), ensemble.name)
        lattice = np.mean(correlator, axis=1)
        std = np.std(correlator, axis=1) / np.sqrt(correlator.shape[1] - 1)
        time = np.linspace(0, int(1.2 * N0half), 1000, endpoint=False)
        p = plt.errorbar(np.arange(N0half), kernel0 * lattice,
                         kernel0 * std, fmt=f"{color}.")
        plt.plot(time, total_int(time),
                 f"{p[0].get_color()}", label=curr_type)
        plt.xlabel(r"$x_0$")
        plt.ylabel(r"$(\alpha/\pi)^2 G(x_0) \cdot \tilde{K}(x_0;m_\mu)$")

    return kernel.integrate(total_int, cutoff)


def execute(ensemble, args, do_jackknife=False):
    """performs an exponential fit, integrates data and plots it.

    Args:
        args (argparse.Namespac): command line arguments
        color (str): color for plot
    """
    if args.kernel == "NLO":
        kernel = NLOKernel
    else: # default args.kernel == "LO":
        args.kernel = "LO"
        kernel = LOKernel

    if args.window == "intermediate":
        a = ensemble.a[0]
        window_parms = (0.4/a, 1.0/a, 0.15/a)
    else:
        window_parms = None

    results = []
    for i in range(len(args.type)):
        base = os.path.join(ensemble.dir, args.type[i])
        correlator = get_correlator(base, ensemble.runname, ensemble.configs,
                                    ensemble.sources)
        corr_jk = jackknife(correlator, axis=1)
        if do_jackknife:
            gm2_jk = np.zeros(corr_jk.shape[0])
            if args.verbose > 0:
                print(f"Performing {corr_jk.shape[0]} jackknife iterations.")
            for j in range(corr_jk.shape[0]):
                if args.verbose > 0 and j % 10 == 0:
                    print(j, end=" ", flush=True)
                gm2_jk[j] = gm2(corr_jk[j, ...], kernel, ensemble, args.type[i],
                                args.cutoff, args.fit_range, plot=False,
                                interpolate=args.interpolate,
                                window_parms=window_parms)
            res, std = jackknife_average(gm2_jk)
            res = uncertainties.ufloat(res, std)
            results.append(res)
            if args.verbose > 0:
                print()
            if args.verbose > -1:
                print(f"{args.type[i]:25}", f"{res:.5e}")

        if args.kernel == "LO":
            result_mean = gm2(correlator, kernel, ensemble, args.type[i], args.cutoff,
                args.fit_range, plot=args.plot, interpolate=args.interpolate,
                window_parms=window_parms)
            if not do_jackknife:
                results.append(result_mean)

    if args.kernel == "LO":
        plt.legend()
        plt.title(
            f"{ensemble.name} {len(ensemble.configs)} cnfgs, {len(args.sources)} srcs"
        )
        if args.save:
            plt.savefig(f"pdfs/integrand_{ensemble.name}_{'_'.join(args.type)}.pdf")
        if args.plot:
            plt.show()

    return results


if __name__ == "__main__":
    args = connected_arguments()

    start = time.time()
    res = execute(args.ensemble, args, True)
    if args.verbose:
        print(f"Time needed: {time.time()-start:.2f} s")
