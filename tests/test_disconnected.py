"""Unit tests for analyzing disconnected contribution."""
import unittest

import numpy as np

from tests.eps import EPS0
from config.directories import ensembles
from disconnected.generate_twopoint import (read_onepoint_splitting,
                                            read_twopoint_splitting, create_twopoint, strip_one)
from disconnected.disconnected import load_twopoint_single


class TestDisconnected(unittest.TestCase):
    """Tests for disconnected contribution."""

    def test_strip_one(self):
        """Recreates one-point function with strip_one
        and compares whether it agrees with original one-point function."""
        ensemble = ensembles["A380a07b324"]
        ensemble.configs = np.arange(10, 50, 10)
        curr_type = "splitting"
        ones = read_onepoint_splitting(ensemble, curr_type)
        ones2 = strip_one(ones)
        for i in range(len(ones)):
            for j in range(4):
                self.assertLess(np.linalg.norm(ones[i][j] - ones2[i][j]), EPS0)

    def test_create_twopoint(self):
        """Loads two-point function and generates two-point function from
        one-point function."""
        ensemble = ensembles["A380a07b324"]
        ensemble.configs = np.arange(10, 50, 10)
        curr_type = "splitting"
        ones = read_onepoint_splitting(ensemble, curr_type)
        twos = read_twopoint_splitting(ensemble, curr_type)
        twos_gen = create_twopoint(ones, ensemble.phys_vol(), fold=False)
        for i in range(len(ensemble.configs)):
            for j in range(5):
                diff = np.linalg.norm(twos[i][j] - twos_gen[i][j])
                self.assertLess(diff, EPS0)

    def test_load_twopoint(self):
        """Loads two-point function and generates two-point function from
        single pieces."""
        ensemble = ensembles["A380a07b324"]
        ensemble.configs = np.arange(20, 270, 10)
        curr_type = "splitting"
        # curr_type = "splitting_wrong_deflation"
        twos = read_twopoint_splitting(ensemble, curr_type, fold=True)
        twos = np.array(twos)
        twos1 = np.sum(twos, axis=1)
        twos1 = np.transpose(twos1)
        twos2 = load_twopoint_single(ensemble, curr_type)
        diff = np.linalg.norm(twos1 - twos2)
        self.assertLess(diff, EPS0)

if __name__ == "__main__":
    unittest.main()
