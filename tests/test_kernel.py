"""see Appendix B.1 in [1].

[1]     M. Della Morte, et al. The hadronic vacuum polarization
        contribution to the muon g-2 from lattice QCD.
        https://arxiv.org/abs/1705.01775 """
import unittest
import numpy as np
from numpy import array, euler_gamma, exp, log, pi
from scipy.integrate import quad

from kernel.kernel import f, window

def part1(t):
    """see Eq. (58) in Ref. [1]."""
    res = pi**2 * t**4 / 9 + \
        pi**2 * t**6  * (120*log(t) + 120*euler_gamma - 169) / 5400 + \
        pi**2 * t**8  * (210*log(t) + 210*euler_gamma - 401) / 88200 + \
        pi**2 * t**10 * (360*log(t) + 360*euler_gamma - 787) / 2916000 + \
        pi**2 * t**12 * (3080*log(t) + 3080*euler_gamma - 7353) / 768398400
    return res


def part2(t):
    """see Eq. (60) in Ref. [1]."""
    res = 2*pi**2 * t**2 - 4*pi**3 * t + 4*pi**2*(4*log(t) + 4*euler_gamma - 1) + \
        8*pi**2/t**2 - 2*pi**2.5 / t**0.5 * exp(-2*t) * ( \
            0.0197159*(1/t - 0.7)**6 - 0.0284086*(1/t - 0.7)**5 + \
            0.0470604*(1/t - 0.7)**4 - 0.107632 *(1/t - 0.7)**3 + \
            0.688813 *(1/t - 0.7)**2 + 4.71371  *(1/t - 0.7) + 3.90388 \
        )
    return res


def paper(t, m_mu):
    t_hat = m_mu * t
    if t_hat < 1.05:
        return part1(t_hat) / m_mu**2
    else:
        return part2(t_hat) / m_mu**2


def get_paper(t_range, m_mu):
    """create kernel for times in t_range."""
    result = []
    for t in t_range:
        result.append(paper(t, m_mu))

    return array(result)

class TestKernel(unittest.TestCase):

    def test_schwinger(self):
        """Test that Schwinger contribution evaluates to
        alpha / (2*pi). """
        schwinger, err = quad(f, 0, np.inf)
        self.assertLess(abs(schwinger - 0.5), err)

    def test_intermediate(self):
        """Tests intermediate window
        """
        theta = window(0.4/0.05, 1.0/0.05, 0.15/0.05)
        testvec = np.array([theta(0), theta(32), theta(0.7/0.05)])
        expvec = np.array([0.01, 0.01, 0.95])
        self.assertLess(np.linalg.norm(testvec-expvec), 0.05)




if __name__ == "__main__":
    unittest.main()
