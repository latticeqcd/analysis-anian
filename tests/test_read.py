import unittest

import numpy as np

from config.directories import ensembles
from disconnected.read_disconnected import read_disconnected_ensemble
from read.read_meson import get_correlator_ensemble

class TestRead(unittest.TestCase):
    def test_read_disconnected(self):
        N0 = 64
        a5d = ensembles["A5d"]
        a5d.configs = np.arange(1, 51, 1)
        gammas = [1,2,3]
        shape = (N0, len(a5d.configs), len(a5d.sources), len(gammas), len(a5d.flavors))
        corr = read_disconnected_ensemble(ensembles["A5d"], "disc_loc_loc", gammas)
        self.assertTrue(np.all(np.shape(corr) ==  shape))

    def test_read_meson(self):
        N0 = 64
        a380 = ensembles["A380a07b324"]
        corr = get_correlator_ensemble(a380, "locloc/f0")
        self.assertTrue(np.all(np.shape(corr) == (N0/2, len(a380.configs))))