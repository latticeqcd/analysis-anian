import unittest

import numpy as np

from fitting.mass import one_parameter_fit, two_parameter_fit, chisquare
from fitting.functions import cosh_fit
from read.read_meson import get_correlator_ensemble
from tests.eps import EPS0, EPS1

class TestFit(unittest.TestCase):

    @unittest.skip("correlated fit does not give good results")
    def test_two_parameter(self):
        from config.directories import ensembles
        ensemble = ensembles["qcd0"]
        correlator = np.real(get_correlator_ensemble(ensemble, "loc_loc"))
        N0 = 2*np.shape(correlator)[0]
        fit_range = np.arange(1, int(N0/2))
        (mass, ampl), cov = two_parameter_fit(correlator, fit_range, diagonal=False)
        
        if False:
            import matplotlib.pyplot as plt
            t = np.linspace(0, int(N0/2)-1, 1000)
            plt.plot(t, cosh_fit(N0, t, mass, ampl))
            print(correlator.shape)
            plt.errorbar(np.arange(int(N0/2)), np.mean(correlator, axis=1), np.std(correlator, axis=1), fmt=".")
            plt.yscale("log")
            plt.show()
        self.assertLessEqual(cov.chisqr, chisquare[len(fit_range)-2-1])

    def test_two_parameter_diagonal(self):
        from config.directories import ensembles
        ensemble = ensembles["qcd0"]
        correlator = get_correlator_ensemble(ensemble, "loc_loc")
        N0 = 2*np.shape(correlator)[0]
        fit_range = np.arange(1, int(N0/2))
        (mass, ampl), cov = two_parameter_fit(np.real(correlator), fit_range, diagonal=True)
        self.assertLessEqual(cov.chisqr, chisquare[len(fit_range)-2-1])
    
    def test_noisy_fit(self):
        from fitting.functions import cosh_fit 
        mass = 0.5
        ncnfg = 90
        N0 = 16
        ampl = 0.002
        t = np.arange(int(N0/2))
        true_corr = cosh_fit(N0, t, mass, ampl)
        correlator = np.zeros((int(N0/2),ncnfg))
        for i in range(ncnfg):
            correlator[:, i] = true_corr * (1 + 0.001*np.random.random(len(true_corr)))

        (mass_fit, ampl_fit), out = two_parameter_fit(correlator, t, diagonal=False)
        fitted2 = cosh_fit(N0, t, mass_fit, ampl_fit)
        res = np.linalg.norm((true_corr - fitted2)/true_corr)
        self.assertLess(out.chisqr, chisquare[len(t)-2-1])

        _, out = one_parameter_fit(correlator, t[:-1], diagonal=False)
        fitted1 = cosh_fit(N0, t, mass_fit, ampl)
        res = np.linalg.norm((true_corr - fitted1)/true_corr)
        self.assertLess(out.chisqr, chisquare[len(t)-2-1])
