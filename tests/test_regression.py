"""Regression tests for data used in overleaf document."""
from argparse import Namespace
import unittest

import numpy as np
from uncertainties import ufloat

from config.directories import ensembles
import connected.gm2
import disconnected.gm2


class TestConnected(unittest.TestCase):
    def test_window(self):
        reference = [
                     ufloat(5.284624384614289e-08, 5.722003484544608e-10),
                     ufloat(5.2476370423158515e-08, 5.213101071776113e-10),
                     ufloat(8.34120286403267e-10, 3.645685691889324e-12),
                     ufloat(3.5810162521784796e-08, 4.088590153036837e-10),
                     ufloat(3.5647600319628975e-08, 3.7249648487837665e-10),
                     ufloat(4.708782783808677e-10, 2.2410273056050076e-12)
                    ]

        ensemble = ensembles["A380a07b324"]
        # ensemble.configs = np.arange(10, 50, 10)
        args = Namespace(fit_range=np.arange(13, 20),
                         cutoff=20,
                         sources=np.arange(10),
                         type=["ju_0_loc_loc", "ju_1_loc_loc", "ju_3_loc_loc",
                               "ju_0_ps_loc", "ju_1_ps_loc", "ju_3_ps_loc"],
                         kernel="LO",
                         interpolate=True,
                         plot=False,
                         save=True,
                         window="intermediate",
                         verbose=0
        )
        results = connected.gm2.execute(ensemble, args, do_jackknife=True)
        for i in range(len(results)):
            self.assertAlmostEqual(results[i].n/reference[i].n, 1.0)
            self.assertAlmostEqual(results[i].s/reference[i].s, 1.0)


class TestDisconnected(unittest.TestCase):
    def test_window(self):
        reference = [ufloat(-5.3361485545730465e-09, 6.061118938399852e-09)]
        args = Namespace(ensemble=ensembles["A380a07b324"],
                         fit_range=np.arange(13, 20),
                         cutoff=31,
                         type=["hopping"],
                         mass=None,
                         suffix_disc="",
                         window="intermediate",
                         verbose=-1
        )
        results = disconnected.gm2.execute(args, plot=False)
        for i in range(len(results)):
            self.assertAlmostEqual(results[i].n/reference[i].n, 1.0)
            self.assertAlmostEqual(results[i].s/reference[i].s, 1.0)


if __name__ == "__main__":
    unittest.main()
