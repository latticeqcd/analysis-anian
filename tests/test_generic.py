"""run with `python -m tests.tests from git root directory"""
import unittest

import numpy as np

from config.constants import M_MU_LATTICE
from disconnected.disconnected import get_disconnected
from kernel.kernel import get_kernel
from kernel.kerneltest import get_paper
from tests.eps import EPS0, EPS1


class TestKernel(unittest.TestCase):

    def test_kernel(self):
        m_mu = M_MU_LATTICE(0.0755)
        t = np.linspace(0.001, 32, 1000)
        scipy = get_kernel(t, m_mu, None, load=False)
        alternativ = get_paper(t, m_mu)
        diff = np.linalg.norm(alternativ-scipy) / np.linalg.norm(scipy)
        self.assertLessEqual(diff, EPS1)

class TestDisconnected(unittest.TestCase):
    
    def test_correlator(self):
        vol = 100
        corr = np.random.random((10, 10, 10, 10))
        g = get_disconnected(corr, vol)



if __name__ == '__main__':
    """python -m tests.tests"""
    unittest.main()