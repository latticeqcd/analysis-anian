"""helper functions to implement the kernel function.

Reference:

[1]     Della Morte et al., The hadronic vacuum polarization
        contribution to the muon g-2 from lattice QCD, 2017.
        https://arxiv.org/abs/1705.01775
[2]     Anian Altherr, Notes about renormalization:
        https://gitlab.com/rcstar/notes-zurich/-/blob/536d41b39f9e3f14fd90e0b3440425ad7b9fe952/pdfs/renormalization.pdf
[3]     Bazavov et al., https://arxiv.org/abs/2301.08274
"""
from abc import ABC, abstractmethod
import itertools
import os

import numpy as np
from scipy.integrate import quad, dblquad
from scipy.interpolate import CubicSpline

from config.constants import ALPHA
from fitting.functions import exp_fit

filedir = os.path.dirname(os.path.realpath(__file__))
kernel_dir = os.path.join(filedir, "kernel_dat")

# pylint: disable=unnecessary-lambda-assignment

# window parameters
window_parms = {"intermediate": lambda a: (0.4/a, 1.0/a, 0.15/a)}

def Z(s_hat):
    """Eq. (46) in Ref. [1].

    Args:
        s_hat (float): dimensionless s/m_mu^2

    Returns:
        float: Z(ŝ)
    """
    return -(s_hat - np.sqrt(s_hat**2 + 4 * s_hat)) / (2 * s_hat)


def f(s_hat):
    """Eq. (45) in Ref. [1]. Notice that we take the dimensionful factor
    1/m_mu^2 to kernel(t, m_mu, **kwargs).

    Args:
        s_hat (float): dimensionless s/m_mu^2

    Returns:
        float: f(ŝ)
    """
    result = s_hat * Z(s_hat) ** 3
    result *= (1 - s_hat * Z(s_hat)) / (1 + s_hat * Z(s_hat) ** 2)
    return result

def g(w, t, m_mu):
    """Auxiliary function for kernel function

    Args:
        w (float): omega
        t (float): time
        m_mu (float): muon mass
    """
    return (w * t * m_mu)**2 - 4 * np.sin(w * t * m_mu / 2)**2


def spline(correlator, cutoff):
    """returns a cubic spline interpolation for times smaller than `cutoff`,

    Args:
        correlator (np.ndarray): correlator, length N0/2.
        cutoff (float): uses exponential for times above cutoff.

    Returns:
        [list]: enhanced correlator
    """

    # cubic spline interpolation on log(correlator)
    valid = np.where(correlator[: cutoff + 1] > 0)[0]
    log_corr = np.log(correlator[valid])
    cubic = CubicSpline(valid, log_corr)

    return np.vectorize(lambda t: np.exp(cubic(t)))


def long_time(mass, ampl):
    """long time behavior of correlator.

    Args:
        correlator (np.ndarray): length N0/2
        mass (float): mass
        ampl (float): amplitude

    """
    return np.vectorize(lambda t: exp_fit(t, mass, ampl))


def filename(t_range, ensemble, m_mu):
    """sets up the filename for the kernel data.

    Args:
        t_range (np.array): time range
        ensemble (str): name of ensemble
        m_mu (float): muon mass

    Returns:
        str: path to filename
    """
    if isinstance(t_range[0], (list, tuple)):
        first, last, num = int(t_range[0][0]), int(t_range[-1][0]), len(t_range)
    else:
        first, last, num = int(t_range[0]), int(t_range[-1]), len(t_range)
    file = f"kernel_{ensemble}_{first}_{last}_{num}_muon_{m_mu:.8g}.dat"
    return os.path.join(kernel_dir, file)


def window(t0, t1, Delta):
    """Implements the window function as in Eq. (2.7) of [3].

    Intermediate window: (t0, t1, Delta) = (0.4, 1.0, 0.15) fm
    W2 window: (t0, t1, Delta) = (1.5, 1.9. 0.15) fm

    Args:
        t0 (int): initial time
        t1 (int): final time
        Delta (int): sharpness

    Returns:
        _type_: _description_
    """
    return lambda t: 0.5 * (np.tanh((t-t0)/Delta) - \
                            np.tanh((t-t1)/Delta) + \
                            np.tanh((-t-t0)/Delta) - \
                            np.tanh((-t-t1)/Delta))


class Kernel(ABC):
    """Functions for evaluating kernel"""


    @classmethod
    @abstractmethod
    def kernel_integrand(cls, w, t, m_mu):
        """Implements the integrand of the kernel function.

        Args:
            w (float): omega
            t (float/list): time(s)
            m_mu (float): muon mass

        Returns:
            float: integrand
        """

    @classmethod
    @abstractmethod
    def parts_integrate(cls, correlator, cutoff, **kwargs):
        """Integrates the correlator for t < cutoff and t >= cutoff

        Args:
            correlator (np.ndarray): size (N0/2)
            cutoff (float): cutoff for lattice data

        Returns:
            (function): integrand for small and large times
        """


    @classmethod
    @abstractmethod
    def integrate(cls, total_int, cutoff):
        """convolutes correlator with kernel.

        Args:
            total_int (function): float -> float or (float, float) -> float
            cutoff (float): cutoff for model, < N0/2
        """

    @classmethod
    def kernel(cls, t, m_mu, **kwargs):
        """Eq. (44) in Ref. [1].

        Args:
            t (float/list): time(s)
            m_mu (float): muon mass
            kwargs (dict): optional arguments that can be passed to
                scipy.integrate.quad

        Returns:
            float: integrand kernel
        """
        I = lambda w: (1/m_mu)**2 * cls.kernel_integrand(w, t, m_mu)
        result, _ = quad(I, 0.0, np.inf, **kwargs)
        return (ALPHA / np.pi)**2 * result


    @classmethod
    def get_kernel(cls, t_range, m_mu, ensemble, load=False):
        """create kernel for times in t_range.
        stores the muon mass at first position and verifies that they agree.

        Args:
            t_range (np.array): time range
            m_mu (float): muon mass
            ensemble (str): ensemble name, only needed if load=True
            load (bool, optional): recalculate kernel. Defaults to False.

        Returns:
            np.array: kernel
        """
        if not os.path.isdir(kernel_dir):
            os.mkdir(kernel_dir)
        try:
            if not load:
                raise FileNotFoundError
            # load data
            file = filename(t_range, ensemble, m_mu)
            with open(file, "rb") as f:
                result = np.load(f, allow_pickle=True)
            if not np.allclose(m_mu, result[0]):
                raise FileNotFoundError
        except FileNotFoundError:
            # calculate data
            result = [m_mu]
            for t in t_range:
                result.append(cls.kernel(t, m_mu))
            result = np.array(result)
            file = filename(t_range, ensemble, m_mu)
            with open(file, "wb") as f:
                np.save(f, result, allow_pickle=True)
        return result[1:]



class LOKernel(Kernel):
    """Leading-order kernel"""

    @classmethod
    def kernel_integrand(cls, w, t, m_mu):
        """Eq. (44) in Ref. [1]. We rescale omega -> omega/m_mu
        (see overleaf notes section 1)

        Args:
            w (float): omega
            t (float): time
            m_mu (float): muon mass

        Returns:
            float: integrand
        """
        integrand = 8 * np.pi**2 * f(w**2) / w * g(w, t, m_mu)
        return integrand

    @classmethod
    def parts_integrate(cls, correlator, cutoff, mass, m_mu,
                        ampl=None, interpolate=True, window_parms=None):
        """compute different parts (small time: lattice data, large time:
        extrapolation)

        Args:
            correlator (np.ndarray): size (N0/2)
            cutoff (float): cutoff for lattice data
            mass (float): for extrapolation
            m_mu (float): muon mass
            window (tuple): expects three values for (t0, t1, Delta)
                in lattice units
            ampl (float): for extrapolation
            kernel (class): kernel class, see kernel.kernel for examples
                            (LOKernel, NLOKernel)
            interpolate (bool): use cubic spline for small times.

        Returns:
            (function, function): integrand for small and large times
        """
        if ampl is None:
            ampl = correlator[cutoff] * np.exp(mass * cutoff)
        if interpolate:
            int1 = lambda t: cls.kernel(t, m_mu) * spline(correlator, cutoff)(t)
        else:
            def int1(t):
                if isinstance(t, (list, np.ndarray)):
                    t = list(map(int, np.round(t)))
                else:
                    t = int(np.round(t))
                # return cls.kernel(t, m_mu) * correlator[list(map(int, np.round(t)))]
                return cls.kernel(t, m_mu) * correlator[t]

        int2 = lambda t: cls.kernel(t, m_mu) * long_time(mass, ampl)(t)
        if window_parms:
            return np.vectorize(lambda t:
                window(*window_parms)(t) * int1(t) if t < cutoff else
                window(*window_parms)(t) * int2(t)
            )
        else:
            return np.vectorize(lambda t: int1(t) if t < cutoff else int2(t))

    @classmethod
    def integrate(cls, total_int, cutoff):
        """convolutes correlator with kernel.

        Args:
            total_int (function): function is time as argument, as returned by parts_integrate
            cutoff (float): cutoff for model, < N0/2
        """
        part1 = quad(total_int, 0, cutoff)[0]
        part2 = quad(total_int, cutoff, np.inf)[0]
        return part1 + part2

    @classmethod
    def integrate_fast(cls, correlator, cutoff, mass, m_mu, ampl):
        part1 = sum([cls.kernel(t, m_mu) * correlator[t] for t in range(1, cutoff)])
        part1 += 0.5 * (cls.kernel(0, m_mu) * correlator[0] + cls.kernel(cutoff, m_mu) * correlator[cutoff])
        int2 = lambda t: cls.kernel(t, m_mu) * long_time(mass, ampl)(t)
        part2 = quad(int2, cutoff, np.inf)[0]
        return part1 + part2


class NLOKernel(Kernel):
    """Next-to-leading-order kernel"""

    @classmethod
    def kernel_integrand(cls, w, t, m_mu):
        """Eq. (1.61) in Ref. [2]. We rescale omega -> omega/m_mu.

        Args:
            w (float): omega
            t (np.array): times [t, t']
            m_mu (float): muon mass in lattice units
        """
        integrand = 32 * np.pi**4 * f(w**2) / w**3 * g(w, t[0], m_mu) * g(w, t[1], m_mu)
        return integrand

    @classmethod
    def get_kernel(cls, t_range, m_mu, ensemble, load=False):
        times = itertools.product(t_range, t_range)
        result = Kernel.get_kernel(times, m_mu, ensemble, load)
        return np.array(result).reshape((len(t_range), len(t_range)))

    @classmethod
    def parts_integrate(cls, correlator, cutoff, mass, m_mu, ampl=None):
        """compute different parts (small time: lattice data, large time:
        extrapolation)

        Args:
            correlator (np.ndarray): size (N0/2)
            cutoff (float): cutoff for lattice data
            mass (float): for extrapolation
            m_mu (float): muon mass
            ampl (float): for extrapolation
            kernel (class): kernel class, see kernel.kernel for examples
                            (LOKernel, NLOKernel)

        Returns:
            (function, function): integrand for small and large times
        """
        if ampl is None:
            ampl = correlator[cutoff] * np.exp(mass * cutoff)
        interpolate = spline(correlator, cutoff)
        long = long_time(mass, ampl)

        def total_int(t):
            ker = cls.kernel(t, m_mu)
            if t[0] < cutoff and t[1] < cutoff:
                return ker * interpolate(t[0]) * interpolate(t[1])
            if t[0] < cutoff and t[1] >= cutoff:
                return ker * interpolate(t[0]) * long(t[1])
            if t[0] >= cutoff and t[1] < cutoff:
                return ker * long(t[0]) * interpolate(t[1])
            if t[0] >= cutoff and t[1] >= cutoff:
                return ker * long(t[0]) * long(t[1])

        return lambda t1, t2: total_int([t1, t2])

    @classmethod
    def integrate(cls, total_int, cutoff):
        """convolutes correlator with kernel.

        Args:
            correlator (np.ndarray): length N0/2
            cutoff (float): cutoff for model, < N0/2
        """
        res = 0
        res += dblquad(total_int, 0, cutoff, lambda x: 0, lambda x: cutoff)[0]
        res += dblquad(total_int, 0, cutoff, lambda x: cutoff, lambda x: np.inf)[0]
        res += dblquad(total_int, cutoff, np.inf, lambda x: 0, lambda x: cutoff)[0]
        res += dblquad(total_int, cutoff, np.inf, lambda x: cutoff, lambda x: np.inf)[0]
        return res



if __name__ == "__main__":
    from config.constants import LATTICE_CONSTANT, M_MU_LATTICE

    t_range = np.linspace(0, 32, 1000)
    a = LATTICE_CONSTANT["qcd0"]
    m_mu = M_MU_LATTICE(a)
    data = LOKernel.get_kernel(t_range, m_mu, "qcd0")
    file = filename(t_range, "qcd0", m_mu)
    with open(file, "wb") as f:
        np.save(f, data, allow_pickle=True)
