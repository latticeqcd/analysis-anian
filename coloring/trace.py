"""Estimates trace of a sparse matrix"""
import numpy as np


def trace_block(A, m, n):
    """Estimates trace of block-diagonal matrix A

    Args:
        A (np.ndarray): shape (m*n, m*n)
        m (int): number of blocks
        n (int): block size
    """
    result = 0
    for k in range(n):
        v = np.zeros(m*n)
        for i in range(m):
            v[i*n+k] = 1
        result += v @ (A @ v)

    assert abs(result - np.trace(A)) < 1e-10, f"failed for m={m}, n={n}"


def off_trace_block(A, m, n, offset=1):
    """Estimates off-set trace of block-diagonal matrix A
        sum_i A[i, i+offset]

    Args:
        A (np.ndarray): shape (m*n, m*n)
        m (int): number of blocks
        n (int): block size
        offset (int): offset
    """
    ref = 0
    mat = np.zeros((m*n, m*n))
    for i in range(m*n):
        ref += A[i, (i+offset) % (m*n)]
        mat[i, (i-1) % (m*n)] = 1

    for i in range(m):
        for j in range(m):
            if i != j:
                mat[i*n:(i+1)*n, j*n:(j+1)*n] = np.random.random((n,n))

    result = 0
    mat2 = 0
    K = n if m == 1 else n-1  # number of probing vectors
    for k in range(K):
        v0 = np.zeros(m*n)
        v1 = np.zeros(m*n)
        for i in range(m):
            v0[i*n+k] = 1
            v1[(i*n+k+1) % (m*n)] = 1
        result += v0 @ (A @ v1)
        mat2 += np.kron(v1.reshape((-1,1)), v0)
    assert abs(result - ref) < 1e-10, f"failed for m={m}, n={n}"


def trace_band(A, b=1, offset=0):
    """Estimates off-set trace of band-diagonal matrix A
        sum_i A[i, i+1]

    Args:
        A (np.ndarray): shape (n, n)
        b (int): banded-ness, i.e. tridiagonal is b=1
    """
    n = A.shape[0]
    assert b>0

    # reference calculation
    mat_ref = np.zeros((n, n))  # matrices with ones on off-diagonal
    ref = 0
    for i in range(n):
        ref += A[(i+offset) % n, i]
        mat_ref[(i+offset) % n, i] = 1

    result = 0
    mat = 0
    nblocks = n // (2*b+1) # number of blocks
    for k in range(2*b+1):
        v0 = np.zeros(n)
        v1 = np.zeros(n)
        for i in range(nblocks):
            v0[(k+offset+i*(2*b+1)) % n] = 1
            v1[k+i*(2*b+1)] = 1
        result += v0 @ (A @ v1)
        mat += np.kron(v0.reshape((-1,1)), v1)

    # remaining vectors
    for k in range((2*b+1)*nblocks, n):
        v0 = np.zeros(n)
        v1 = np.zeros(n)
        v0[(k+offset) % n] = 1
        v1[k] = 1
        result += v0 @ (A @ v1)
        mat += np.kron(v0.reshape((-1,1)), v1)

    mat_ref[np.where(A==0)] = 0
    mat[np.where(A==0)] = 0
    print(mat)
    assert np.linalg.norm(mat_ref - mat) < 1e-10
    assert abs(result - ref) < 1e-10, f"{ref} != {result}"
    return mat


def create_band_diagonal(n, b):
    """Creates a band-diagonal matrix with bandwidth b

    Args:
        n (int): dimension of matrix
        b (int): bandwidth, b=0 is diagonal, b=1 is tridiagonal

    Returns:
        np.ndarray: shape (n, n)
    """
    arrs = [np.random.random(n)]
    offsets = [0]
    for i in range(1, b+1):
        arrs.extend([np.random.random(n-i), np.random.random(n-i)])
        offsets.extend([i, -i])
    A = diags(arrs, offsets).toarray()
    for i in range(b):
        for j in range(i+1):
            A[n-1-i+j, j] += np.random.random()
            A[j, n-1-i+j] += np.random.random()
    return A


if __name__ == "__main__":
    from scipy.sparse import diags
    np.random.seed(13)
    n, b = 8, 2
    A = create_band_diagonal(n, b)
    with np.printoptions(linewidth=200):
        trace_band(A, b, offset=1)


    # for m in range(1, 10): # number of blocks
    #     for n in range(1, 10): # block size
    #         A = np.zeros((m*n, m*n))
    #         for i in range(m):
    #             A[i*n:(i+1)*n, i*n:(i+1)*n] = np.random.random((n, n))

    #         trace(A, m, n)
    #         off_trace(A, m, n)
