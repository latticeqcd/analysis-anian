# Usage examples

Set up the directories where data is stored in `config/directories.py`.
It is assumed to follow a structure such as

dat
├── A360a50b324
│   ├── ju_0_loc_loc
│   ├── ju_0_ps_loc
│   └── ju_pion_ps_loc
├── A400a00b324
|   ├── pion
|   └── pion_scalar
├── qcd
│   ├── disc_loc_loc
│   └── loc_loc
└── qxd
    ├── disc_loc_loc
    └── loc_loc

That is for each ensemble you have subdirectories for different flavors and
types. The actual names of the subdirectories do not matter.

- Fit mass
```python -m fitting.mass --ensemble C380a50b324 --type ju_0_loc_loc```

- calculate g-2
```python -m connected.gm2 --ensemble C380a50b324 --type ju_0_loc_loc```

- Compare correlators
- ```python -m plot.correlator --ensemble qcd0 --configs 5 100 5 --type loc_loc --type loc_loc```

- Run tests
```python -m unittest```.

- disconnected
```
python -m disconnected.disconnected --ensemble A380a07b324 --type disc_imaginary --configs 10 510 10 --sources 100
```
