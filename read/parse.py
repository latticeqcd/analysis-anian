"""functions for reading arguments from command line"""
import os
from fractions import Fraction
from argparse import Action, ArgumentParser

import numpy as np

from config.directories import ensembles


class RangeAction(Action):
    """Given an argument '--range a b step', stores it as
    np.arange(a, b, step)."""

    def __call__(self, parser, namespace, values, option_string):
        setattr(namespace, self.dest, np.arange(*values))

class RationalAction(Action):
    def __call__(self, parser, namespace, values, option_string):
        items = getattr(namespace, self.dest, None)
        if items is None:
            items = []
        else:
            items = items[:]
        items.append(float(Fraction(*values)))
        setattr(namespace, self.dest, items)

def arguments_plot():
    """parse arguments for plotting."""
    parser = ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--dir", type=str, help="Path to dat files", action="append")
    group.add_argument("--ensemble", type=str, help="Ensemble" " used, e.g., A5a, E5a.")
    parser.add_argument(
        "--type",
        type=str,
        help="flavor and current type, e.g.," "ju_0_loc_loc",
        action="append",
    )
    parser.add_argument("--runname", type=str, help="runname")
    parser.add_argument("--configs", type=int, nargs="*", action=RangeAction)
    parser.add_argument("--suffix", type=str, action="append")
    parser.add_argument(
        "--sources", type=int, nargs="*", action=RangeAction, default=np.arange(10)
    )
    args = parser.parse_args()
    if args.dir is None and args.type is None:
        parser.error("Either dir or ensemble must be given")
    if args.dir is None:
        args.dir = []
        ens = ensembles[args.ensemble]
        args.runname = ens.runname
        if args.configs is None:
            args.configs = ens.configs
        for type in args.type:  # pylint: disable=redefined-builtin
            args.dir += [os.path.join(ens.dir, type)]

    if args.runname is None:
        parser.error("runname not set")

    return args

def base_args(parser):
    """base arguments"""
    parser.add_argument("--ensemble", type=str,
                        help="Ensemble" " used, e.g., A5a, E5a.", required=True)
    parser.add_argument("--dir", type=str, help="Path to dat files")
    parser.add_argument("--type", type=str, action="append",
        required=True, help="flavor and current type, e.g.," "ju_0_loc_loc")
    parser.add_argument("--runname", type=str, help="runname")
    parser.add_argument("--configs", type=int, nargs="*", action=RangeAction)
    parser.add_argument("--sources", type=int, nargs="*", action=RangeAction)
    parser.add_argument("--save", type=str, help="filename for plot")
    parser.add_argument("--verbose", type=int, default=1)
    parser.add_argument("--window", type=str, default=None,
                        help="window method, currently supported: intermediate")
    # parser.add_argument("--cutoff", type=int, help="Cutoff for integrand")
    return parser

def process_base_args(args):
    """modifies base arguments"""
    args.ensemble = ensembles[args.ensemble]
    if args.configs is not None:
        args.ensemble.configs = args.configs
    else:
        args.configs = args.ensemble.configs
    if args.sources is not None:
        args.ensemble.sources = args.sources
    if args.runname is not None:
        args.ensemble.runname = args.runname
    if args.dir is not None:
        args.ensemble.dir = args.dir
    return args

def connected_arguments():
    """parse arguments for connected correlator."""
    parser = ArgumentParser()
    parser = base_args(parser)
    parser.add_argument("--fit-range", type=int, nargs="*", action=RangeAction,
                        required=True)
    parser.add_argument("--cutoff", type=int, required=True)
    parser.add_argument("--interpolate", type=bool, default=True,
                        help="Use cubic spline interpolation for small times")
    parser.add_argument("--kernel", type=str, default="LO",
                        help="Kernel type (LO, NLO)")
    parser.add_argument("--plot", type=int, default=True)
    # parser.add_argument("--flavor-charge", type=str, nargs="*",
    #                     action=RationalAction, required=True)
    args = parser.parse_args()
    if args.sources is None:
        raise parser.error("--sources required")
    # if len(args.type) != len(args.flavor_charge):
        # raise parser.error("There need to be as many flavor charges as current types.")
    return process_base_args(args)

def extrapolate_arguments():
    """parse arguments for disconnected.extrapolate"""
    parser = ArgumentParser()
    parser = base_args(parser)
    parser.add_argument("--type-conn", type=str, action="append",
        help="flavor and current type for connected contribution",
        required=True)
    # parser.add_argument("--flavor-charge", type=str, nargs="*",
    #                     action=RationalAction, required=True)
    args = parser.parse_args()
    # if len(args.type_conn) != len(args.flavor_charge):
    #     raise parser.error(f"There need to be as many flavor charges as current types: {args.type_conn}, {args.flavor_charge}")
    return process_base_args(args)


def disconnected_arguments():
    parser = ArgumentParser()
    parser = base_args(parser)
    parser.add_argument("--cutoff", type=int, required=True)
    parser.add_argument("--mass", type=float, help="Vector meson mass")
    parser.add_argument("--type-conn", type=str, action="append",
        help="flavor and current type for connected contribution")
    parser.add_argument("--suffix-disc", type=str, default="", help="For backward compatibility: Use '' or '_c0'")
    parser.add_argument("--suffix-conn", type=str, default="", help="For backward compatibility: Use '' or '_t33'")
    args = parser.parse_args()
    return process_base_args(args)

def disconnected_twopoint_arguments():
    """parse arguments for integrating."""
    parser = ArgumentParser()
    parser = base_args(parser)
    parser.add_argument("-  -suffix", type=str, action="append",
        help="e.g. '', '_c0', '_c0_local_local'")
    args = parser.parse_args()
    return process_base_args(args)


def renormalization_arguments():
    """parse arguments for integrating."""
    parser = ArgumentParser()
    parser = base_args(parser)
    parser.add_argument("--fit-range", type=int, nargs="*", action=RangeAction,
                        required=True)
    args = parser.parse_args()
    return process_base_args(args)



def arguments():
    """parse arguments for integrating."""
    parser = ArgumentParser()
    parser = base_args(parser)
    args = parser.parse_args()
    return process_base_args(args)
