"""Provides utility functions to read binary files."""
import struct

def write_int(file, arr):
    """writes array of integers to file in little-endian
    as 4 bytes numbers

    Args:
        file (_io.TextIOWrapper): open file handler (e.g. file = open(filename, mode))
        arr (np.array): array containing ints
    """
    for el in arr:
        file.write(struct.pack('<I', el))

def write_double(file, arr):
    """Writes array of doubles to file in little-endian
    as 8 bytes numbers

    Args:
        file (_io.TextIOWrapper): open file handler (e.g. file = open(filename, mode))
        arr (np.array): array containing doubles
    """
    for el in arr:
        file.write(struct.pack('<d', el))

def read_int(file, n):
    """
    file: open file pointer.
    n: number of integers to read."""
    int_size = 4
    result =  struct.unpack(f'<{n}I', file.read(n*int_size))
    if n == 1:
        return result[0]
    return result

def read_double(file, n):
    """
    file: open file pointer.
    n: number of integers to read."""
    double_size = 8
    result =  struct.unpack(f'<{n}d', file.read(n*double_size))
    if n == 1:
        return result[0]
    return result
